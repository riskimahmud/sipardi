<?php
class Email_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function kirim_email($tujuan, $judul, $content)
	{
		$pesan = '
			<!DOCTYPE html>
			<html>

			<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title>SIPARDI</title>
				<!-- Tell the browser to be responsive to screen width -->
				<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
				<!-- Google Font -->
				<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
				<style>
					body {
						margin: 0;
						padding: 0;
						font-family: "Roboto", sans-serif;
						font-size: 20px;
						background-color: #eee;
						color: rgb(25, 165, 102);
					}

					header {
						text-align: center;
						margin: 30px auto;
					}

					header img {
						width: 200px;
					}

					main {
						/* margin: 0 auto;
						border: 2px solid rgb(25, 165, 102); */
						background-color: #fff;
						max-width: 80%;
						display: block;
						margin: 20px auto;
						padding: 20px;
						border-radius: 20px;
						border: 1px solid rgb(25, 165, 102);
						box-shadow: 0 0 2px 2px rgba(25, 165, 102, .6);
					}

					.title {
						font-size: 1.5em;
						font-weight: bold;
						display: block;
						text-align: center;
						padding-bottom: 10px;
						border-bottom: 1px dashed rgb(25, 165, 102);
						margin-bottom: 20px;
					}

					.content a {
						color: inherit;
						font-weight: bold;
					}

					*{
						margin-bottom: 10px;
					}

					hr{
						border: 0;
						border-bottom: 1px dashed rgb(25, 165, 102);
					}

					h5{
						display: block;
						width: 100%;
					}

					table.produk{
						width: 100%;
						border: 1px solid green;
						border-collapse: collapse;
					}
					
					table.produk th{
						background-color: rgb(25, 165, 102);
						color: white;
						padding:5px;
						font-size: .8em;
						border: 1px solid green;
						border-collapse: collapse;
					}
					
					table.produk td{
						padding: 5px;
						font-size: .7em;
						border: 1px solid green;
						border-collapse: collapse;
					}

					table#keterangan{
						font-size: .7em;
						color: inherit;
					}
					
					table#keterangan td{
						border: 0;
						padding: 5px;
					}
					
					table#keterangan td.value{
						font-weight: bold;
					}

					table#keterangan td.value::before{
						content: ": ";
					}

					a.button{
						display: block;
						width : 50%;
						margin: 0 auto;
						background-color: rgba(25, 165, 102, .8);
						color: white;
						text-align: center;
						text-decoration: none;
						padding: 10px 0;
						font-size: 1.1em;
					}

					footer{
						width: 100%;
						background-color: rgb(25, 165, 102);
						margin-top: 20px;
						padding: 20px 0;
						text-align: center;
						color:white;
						font-size: .8em;
					}

					hr.footer{
						border: 0;
						border-bottom: 1px dashed white;
					}

					footer .social-media{
						margin-top: 20px;
					}
					
					footer .social-media img{
						margin-top: 20px;
						width: 50px;
					}
				</style>
			</head>

			<body>
				<header>
					<img src="' . base_url() . 'assets/img/top-logo.png" alt="Logo Sipardi">
				</header>
				<main>
					<div class="title">' . $judul . '</div>
					<div class="content">
						' . $content . '
					</div>
					<footer>
						Semua ada di sipardi.
						<hr class="footer">
						<div class="social-media">
							Ikuti Kami.<br>
							<a href="https://facebook.com/sipardi.id/"><img src="' . base_url("assets/img/social/facebook.png") . '"></a>							
							<a href="https://instagram.com/sipardi.id"><img src="' . base_url("assets/img/social/instagram.png") . '"></a>							
							<a href="https://wa.me/+6282187468505"><img src="' . base_url("assets/img/social/whatsapp.png") . '"></a>					
						</div>
					</footer>
				</main>
			</body>

			</html>';
		$this->load->library('email');
		$this->email->from("cs@sipardi.id", "Web Pasar SiPARDI - Sistem Pasar Rakyat Digital");
		$this->email->to($tujuan);
		$this->email->subject($judul);
		$this->email->message($pesan);
		// return $pesan;
		return $this->email->send();
		// if (!$this->email->send()) {
		// 	show_error($this->email->print_debugger());
		// } else {
		// 	echo 'Success to send email';
		// }
	}
}
