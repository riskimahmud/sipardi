<?php
class Crud_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	// Cek id
	function cek_id($table, $field)
	{
		$this->db->select('*');
		$this->db->order_by($field, "DESC");
		$this->db->limit(1);
		$cek = $this->db->get($table)->row();
		if (empty($cek)) {
			return "1";
		} else {
			$data	=	$cek->$field + 1;
			return $data;
		}
	}

	// cek data kalau sudah ada atau belum dengan select beberapa field
	function cek_data_existing($table, $field, $id)
	{
		$this->db->select('*');
		$this->db->where($field, $id);
		$cek = $this->db->get($table)->row();
		if (empty($cek)) {
			return false;
		} else {
			return true;
		}
	}

	// cek data kalau sudah ada atau belum
	function cek_data($table, $field, $id)
	{
		$this->db->select('*');
		$this->db->where($field, $id);
		$cek = $this->db->get($table)->row();
		if (empty($cek)) {
			return true;
		} else {
			return false;
		}
	}

	// cek data kalau sudah ada atau belum dengan where array
	function cek_data_where_array($table, $where)
	{
		$this->db->select('*');
		$this->db->where($where);
		$cek = $this->db->get($table)->row();
		if (empty($cek)) {
			return true;
		} else {
			return false;
		}
	}

	// cek data kalau sudah ada atau belum 
	function cek_data_ex($table, $field, $id, $ex_field, $ex_id)
	{
		$this->db->select('*');
		$this->db->where($field, $id);
		$this->db->where($ex_field, $ex_id);
		$cek = $this->db->get($table)->row();
		if (empty($cek)) {
			return true;
		} else {
			return false;
		}
	}

	// ======== ********************************************************************** ==============

	// select
	function select_all($table)
	{
		$this->db->select('*');
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where order
	function select_all_where_order($table, $where, $whereby, $order, $order_by, $limit = 0)
	{
		$this->db->select('*');
		$this->db->where($where, $whereby);
		$this->db->order_by($order, $order_by);
		if ($limit > 0) {
			$this->db->limit($limit);
		}
		$query = $this->db->get($table);
		return $query->result();
	}

	// select order
	function select_all_order($table, $field, $by)
	{
		$this->db->select('*');
		$this->db->order_by($field, $by);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where
	function select_all_where($table, $field, $key)
	{
		$this->db->select('*');
		$this->db->where($field, $key);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where array
	function select_all_where_array($table, $where)
	{
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where array row
	function select_all_where_array_row($table, $where)
	{
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where group
	function select_all_where_group($table, $field, $key, $group)
	{
		$this->db->select('*');
		$this->db->where($field, $key);
		$this->db->group_by($group);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where limit
	function select_all_where_limit($table, $field, $key, $order, $order_by, $limit)
	{
		$this->db->select('*');
		$this->db->where($field, $key);
		$this->db->order_by($order, $order_by);
		$this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where array limit
	function select_all_where_array_limit($table, $where, $order, $orderby, $limit)
	{
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $orderby);
		$this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select where array order
	function select_all_where_array_order($table, $where, $order, $orderby, $limit = 0)
	{
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $orderby);
		if ($limit > 0)
			$this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result();
	}

	function select_one($table, $field, $key)
	{
		$this->db->select('*');
		$this->db->where($field, $key);
		$query = $this->db->get($table);
		return $query->row();
	}

	// select row array *** untuk update userdata user
	function select_one_row_array($table, $field, $key)
	{
		$this->db->select('*');
		$this->db->where($field, $key);
		$query = $this->db->get($table);
		return $query->row_array();
	}

	function select_one_where_array($table, $where)
	{
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->row();
	}

	// select  kembalian jumlah data
	function select_all_num_row($table)
	{
		$this->db->select('*');
		$query = $this->db->get($table);
		return $query->num_rows();
	}

	// select where kembalian jumlah data
	function select_all_where_num_row($table, $field, $key)
	{
		$this->db->select('*');
		$this->db->where($field, $key);
		$query = $this->db->get($table);
		return $query->num_rows();
	}

	// select where array
	function select_all_where_array_num_row($table, $where)
	{
		$this->db->select('*');
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->num_rows();
	}

	// select max
	function select_max($table, $field, $where, $key)
	{
		$this->db->select_max($field);
		$this->db->where($where, $key);
		$cek = $this->db->get($table)->row();
		if ($cek == NULL) {
			return "1";
		} else {
			$data	=	$cek->$field + 1;
			return $data;
		}
	}

	// select beberapa field semua
	function select_by_field($table, $field, $where, $order = null, $order_by = null, $limit = null, $start = null)
	{
		$this->db->select($field);
		if ($order !== null) {
			$this->db->order_by($order, $order_by);
			$this->db->limit($limit, $start);
		}
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->result();
	}

	// select beberapa field baris
	function select_by_field_row($table, $field, $where)
	{
		$this->db->select($field);
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query->row();
	}

	// select like
	function select_like($table, $field, $key)
	{
		$this->db->select("*");
		$this->db->like($field, $key);
		$query = $this->db->get($table);
		return $query->result();
	}

	// Select paginasi
	function select_paging($table, $number, $offset)
	{
		return $query = $this->db->get($table, $number, $offset)->result();
	}

	// Select paginasi where array
	function select_paging_where_array($table, $where, $number, $offset)
	{
		return $query = $this->db->get($table, $number, $offset)->result();
	}

	// limit for paginasi
	public function select_paging_where($table, $where, $limit, $start, $order = null, $likes = null, $join = null)
	{
		if ($likes !== null) {
			$no = 0;
			foreach ($likes as $key => $value) {
				// if ($no == 0) {
				$this->db->like($key, $value);
				// } else {
				// 	$this->db->or_like($key, $value);
				// }
				// $no++;
			}
		}

		if ($order !== null) {
			$this->db->order_by($order["key"], $order["value"]);
		}

		$this->db->where($where);
		// $this->db->from($table);
		// $query = $this->db->get();
		if ($join == 1) {
			// $this->db->from($table);
			$this->db->from($table);
			$this->db->join("rb_kategori_produk", $table . ".id_kategori_produk = rb_kategori_produk.id_kategori_produk");
			$this->db->limit($limit, $start);
			$query = $this->db->get();
		} else {
			$query = $this->db->get($table, $limit, $start);
		}

		return $query->result();
	}

	// select where array pagging with likes
	function select_all_where_array_likes_num_row($table, $where, $likes = null, $join = null)
	{
		$this->db->select('*');
		$this->db->where($where);
		if ($likes !== null) {
			$no = 0;
			foreach ($likes as $key => $value) {
				// if ($no == 0) {
				$this->db->like($key, $value);
				// } else {
				// 	$this->db->or_like($key, $value);
				// }
				// $no++;
			}
		}
		// $query = $this->db->get($table);
		if ($join == 1) {
			// $this->db->from($table);
			$this->db->from($table);
			$this->db->join("rb_kategori_produk", $table . ".id_kategori_produk = rb_kategori_produk.id_kategori_produk");
			$query = $this->db->get();
		} else {
			$query = $this->db->get($table);
		}
		return $query->num_rows();
	}

	// select sum
	function select_sum($table, $field, $where, $join = null)
	{
		$this->db->select_sum($field);
		$this->db->where($where);
		$this->db->from($table);
		if ($join !== null) {
			$this->db->join($join["table"], $table . "." . $join["key"] . " = " . $join["table"] . "." . $join["key"]);
		}
		$query = $this->db->get();
		$data	=	$query->row();
		return $data->$field;
	}


	function select_custom($query)
	{
		$q	=	$this->db->query($query);
		return $q->result();
	}

	function select_custom_num_row($query)
	{
		$q	=	$this->db->query($query);
		return $q->num_rows();
	}

	function select_custom_row($query)
	{
		$q	=	$this->db->query($query);
		return $q->row();
	}

	function custom_query($query)
	{
		$q	=	$this->db->query($query);
		if ($q) {
			return true;
		} else {
			return false;
		}
	}

	function insert($table, $data)
	{
		$q	=	$this->db->insert($table, $data);
		if ($q) {
			return true;
		} else {
			return false;
		}
	}

	function insert_batch($table, $data)
	{
		$q	=	$this->db->insert_batch($table, $data);
		if ($q) {
			return true;
		} else {
			return false;
		}
	}

	function update($table, $data, $key, $id)
	{
		$this->db->where($key, $id);
		$q	=	$this->db->update($table, $data);
		if ($q) {
			return true;
		} else {
			return false;
		}
	}

	function update_where_array($table, $data, $where)
	{
		$this->db->where($where);
		$q	=	$this->db->update($table, $data);
		if ($q) {
			return true;
		} else {
			return false;
		}
	}

	// hapus id
	function hapus_id($table, $field, $id)
	{
		$this->db->where($field, $id);
		$del	=	$this->db->delete($table);
		if ($del) {
			return true;
		} else {
			return false;
		}
	}

	// hapus where array
	function hapus_array($table, $where)
	{
		$this->db->where($where);
		$del	=	$this->db->delete($table);
		if ($del) {
			return true;
		} else {
			return false;
		}
	}
}
