<?php
$pesan = '
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIPARDI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: "Roboto", sans-serif;
            font-size: 15px;
            background-color: #eee;
            color: rgb(25, 165, 102);
        }

        header {
            text-align: center;
            margin: 30px auto;
        }

        header img {
            width: 200px;
        }

        main {
            /* margin: 0 auto;
            border: 2px solid rgb(25, 165, 102); */
            background-color: #fff;
            max-width: 80%;
            display: block;
            margin: 20px auto;
            padding: 20px;
            border-radius: 20px;
            border: 1px solid rgb(25, 165, 102);
            box-shadow: 0 0 2px 2px rgba(25, 165, 102, .6);
        }

        .title {
            font-size: 1.8em;
            font-weight: bold;
            display: block;
            text-align: center;
            padding-bottom: 10px;
            border-bottom: 1px dashed rgb(25, 165, 102);
            margin-bottom: 20px;
        }

        .content a {
            color: inherit;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <header>
        <img src="<?= base_url("assets/img/top-logo.png"); ?>" alt="Logo Sipardi">
    </header>
    <main>
        <div class="title">Verifikasi Akun</div>
        <div class="content">
            Selamat!. Anda berhasil mendaftar di SIPARDI. Silhakan klik link berikut ini.
            <a href="#"> Klik disini </a>untuk melakukan verifikasi email anda.
        </div>
    </main>
</body>

</html>';
return $pesan;
