<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest"> -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/fonts/font.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/sipardi.css'); ?>">
    <!-- <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>"> -->
    <link rel="stylesheet" href="<?= base_url('assets/css/modern-bussines.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/custom/style.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/fontawesome/css/all.css'); ?>">

    <!-- owl carousel -->
    <link rel="stylesheet" href="<?= base_url('assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/owl.theme.default.min.css'); ?>">

    <!-- datepicker -->
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-datepicker3.standalone.min.css'); ?>">

    <!-- datatable -->
    <link rel="stylesheet" href="<?= base_url('assets/datatables/datatables.min.css'); ?>">

    <!-- select2 -->
    <link rel="stylesheet" href="<?= base_url('assets/css/select2.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/select2-bootstrap4.css'); ?>">

    <!-- slick -->
    <link rel="stylesheet" href="<?= base_url('assets/slick/slick.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/slick/slick-theme.css'); ?>">

    <!-- animated -->
    <link rel="stylesheet" href="<?= base_url('assets/css/animate.min.css'); ?>">

    <title>SIPARDI</title>
    <script>
        const url = '<?php echo base_url(); ?>';
    </script>
</head>

<body class="d-flex flex-column main">
    <!-- <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v7.0" nonce="MfY1dplz"></script> -->