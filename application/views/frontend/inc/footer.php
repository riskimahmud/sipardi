<footer class="mt-5" id="sticky-footer">
    <div class="container">
        <div class="row text-white">
            <div class="col-md-3 col-6">
                <h5 class="font-weight-bold">SIPARDI</h5>
                <ul class="list-unstyled">
                    <li><a href="about" class="text-decoration-none text-white">Tentang Sipardi</a></li>
                    <li><a class="text-decoration-none text-white">Pasar</a></li>
                    <li><a href="<?= base_url('blog'); ?>" class="text-decoration-none text-white">Blog</a></li>
                    <li><a href="<?= base_url('promo'); ?>" class="text-decoration-none text-white">Promo</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-6">
                <h5 class="font-weight-bold">Beli</h5>
                <ul class="list-unstyled">
                    <li><a href="cara-beli" class="text-decoration-none text-white">Cara Beli</a></li>
                    <li><a class="text-decoration-none text-white">Pembayaran</a></li>
                    <li><a class="text-decoration-none text-white">Hot List</a></li>
                    <li><a class="text-decoration-none text-white">Kategori</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-6">
                <h5 class="font-weight-bold">Jual</h5>
                <ul class="list-unstyled">
                    <li><a class="text-decoration-none text-white">Mitra Jual</a></li>
                    <li><a href="cara-jual" class="text-decoration-none text-white">Cara Jual</a></li>
                    <li><a class="text-decoration-none text-white">Verifikasi Pasar</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-6">
                <h5 class="font-weight-bold">Ikuti Kami</h5>
                <ul class="list-inline p-0 m-0">
                    <li class="list-inline-item"><a href="https://www.facebook.com/sipardi.id" target="_blank"><img src="<?= base_url("assets/img/social/facebook.png"); ?>" class="img-fluid" width="30" alt="Facebook"></a></li>
                    <li class="list-inline-item"><a href="https://instagram.com/sipardi.id" target="_blank"><img src="<?= base_url("assets/img/social/instagram.png"); ?>" class="img-fluid" width="30" alt="instagram"></a></li>
                    <!--<li class="list-inline-item"><a><img src="<?= base_url("assets/img/social/twitter.png"); ?>" class="img-fluid" width="30" alt="twitter"></a></li>-->
                    <li class="list-inline-item"><a href="https://wa.me/+6282187468505" target="_blank"><img src="<?= base_url("assets/img/social/whatsapp.png"); ?>" class="img-fluid" width="30" alt="whatsapp"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.container -->
</footer>