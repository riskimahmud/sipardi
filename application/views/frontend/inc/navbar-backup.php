<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-white align-items-start" id="navbar-top">
    <a class="navbar-brand" href="<?= base_url(); ?>">
        <img src="<?= base_url('assets/img/logo.png'); ?>" alt="Sipardi" class="logo-brand">
    </a>
    <button class="navbar-toggler text-primary" type="button" data-trigger="#main_nav">
        <span class="fas fa-bars text-primary"></span>
    </button>
    <div class="navbar-collapse" id="main_nav">

        <div class="offcanvas-header mt-3">
            <button class="btn btn-outline-danger btn-close float-right"> &times Close </button>
            <h5 class="py-2 text-white">Main navbar</h5>
        </div>

        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown"> More items </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="#"> Submenu item 1</a></li>
                    <li><a class="dropdown-item" href="#"> Submenu item 2 </a></li>
                </ul>
            </li>
            <li class="nav-item d-block w-100">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" name="keyword" placeholder="Saya mau belanja...">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" name="cari" value="cari" type="submit" id="button-addon2"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a class="nav-link" href="#"> Menu item </a></li>
            <li class="nav-item"><a class="nav-link" href="#"> Menu item </a></li>
            <li class="nav-item dropdown">
                <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown"> Dropdown right </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a class="dropdown-item" href="#"> Submenu item 1</a></li>
                    <li><a class="dropdown-item" href="#"> Submenu item 2 </a></li>
                </ul>
            </li>
        </ul>
    </div> <!-- navbar-collapse.// -->
</nav>