<div class="pt-5">
    <?php if (!$this->uri->segment(1) || ($this->uri->segment(1) == "frontend" && !$this->uri->segment(2))) { ?>
        <header class="mb-5 d-none d-lg-block"></header>
        <!-- <header class="d-none d-lg-block">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active" style="background-image: url('<?= base_url("uploads/slider/1.jpg"); ?>')">
                    </div>
                    <div class="carousel-item" style="background-image: url('<?= base_url("uploads/slider/2.jpg"); ?>')">
                    </div>
                    <div class="carousel-item" style="background-image: url('<?= base_url("uploads/slider/3.jpg"); ?>')">
                    </div>
                </div>
            </div>
        </header> -->
    <?php } else { ?>
        <header class="bg-primary text-white">
            <div class="container text-center">
                <h3><?= $title; ?></h3>
                <?php if (!empty($subtitle)) { ?>
                    <p class="lead"><?= $subtitle; ?></p>
                <?php } ?>
            </div>
        </header>
    <?php } ?>
</div>