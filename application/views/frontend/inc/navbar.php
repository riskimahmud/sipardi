<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-white align-items-start" id="navbar-top">
    <!-- <div class="container"> -->
    <div class="w-25">
        <div class="row align-items-start">

            <a class="navbar-brand " href="<?= base_url(); ?>">
                <img src="<?= base_url('assets/img/logo.png'); ?>" alt="Sipardi" class="logo-brand d-none d-sm-block ml-5">
                <img src="<?= base_url('assets/img/icon.png'); ?>" alt="Sipardi" class="logo-brand d-block d-sm-none ml-4">
            </a>
            <button class="navbar-toggler text-primary ml-auto mr-0" type="button" data-trigger="#navKategori" id="btn-kategori">
                <span class="fas fa-list text-primary"></span>
            </button>
            <div class="navbar-collapse" id="navKategori">

                <div class="offcanvas-header mt-3">
                    <button class="btn btn-outline-danger btn-close float-right"> &times Close </button>
                    <h5 class="py-2 text-black">Pilih Kategori</h5>
                </div>

                <ul class="navbar-nav ml-auto mr-5">
                    <li class="nav-item dropdown">
                        <a class="nav-link  dropdown-toggle" href="#" data-toggle="dropdown"> Kategori </a>
                        <?= getListKategori(); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="w-50 p-0 m-0" style="z-index: inherit;">
        <?= form_open(base_url("produk"), ["method" => "get", "autocomplete" => "off"]); ?>
        <div class="input-group input-group-sm">
            <input type="text" class="typeahead form-control" id="tags" name="keyword" placeholder="Saya mau belanja...">
            <div class="input-group-append">
                <button class="btn btn-secondary" name="cari" value="cari" type="submit" id="button-addon2"><i class="fas fa-search"></i></button>
            </div>
        </div>
        <small class="d-none d-lg-block">
            <ul class="list-inline p-0 m-0">
                <li class="list-inline-item pt-1 m-0">
                    <a class="text-decoration-none mr-4" href="<?= base_url("produk?keyword=ikan&cari=cari"); ?>">Ikan</a>
                </li>
                <li class="list-inline-item pt-1 m-0">
                    <a class="text-decoration-none mr-4" href="<?= base_url("produk?keyword=sayur&sortir=&cari=Cari"); ?>">Sayur</a>
                </li>
                <li class="list-inline-item pt-1 m-0">
                    <a class="text-decoration-none mr-4" href="<?= base_url("produk?keyword=rica&sortir=&cari=Cari"); ?>">Rica</a>
                </li>
                <li class="list-inline-item pt-1 m-0">
                    <a class="text-decoration-none mr-4" href="<?= base_url("produk?keyword=tomat&sortir=&cari=Cari"); ?>">Tomat</a>
                </li>
                <li class="list-inline-item pt-1 m-0">
                    <a class="text-decoration-none mr-4" href="<?= base_url("produk?keyword=bawang&sortir=&cari=Cari"); ?>">Bawang</a>
                </li>
                <li class="list-inline-item pt-1 m-0">
                    <a class="text-decoration-none mr-4" href="<?= base_url("produk?keyword=telur&sortir=&cari=Cari"); ?>">Telur</a>
                </li>
                <li class="list-inline-item pt-1 m-0">
                    <a class="text-decoration-none mr-4" href="<?= base_url("produk?keyword=beras&sortir=&cari=Cari"); ?>">Beras</a>
                </li>
            </ul>
        </small>
    </div>
    <?= form_close(); ?>
    <?php if ($this->session->userdata("user")) {
    ?>
        <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="text-primary"><i class="fas fa-bars"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown notifikasiList">
                    <a class="nav-link" title="Notifikasi" href="#" id="navbarDropdownNotifikasi" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell fa-fw position-relative">
                            <?php if (notifikasiCount() > "0") : ?>
                                <span class="badge badge-danger position-absolute rounded-circle" id="badgeNotifikasi">
                                    <?= notifikasiCount(); ?>
                                </span>
                            <?php endif; ?>
                        </i>
                        <span class="d-inlin-block d-lg-none"> Notifikasi</span>
                    </a>
                    <div class="dropdown-menu p-0 dropdown-menu-right " aria-labelledby="navbarDropdownNotifikasi">
                        <div class="dropdown-item p-0">
                            <?php
                            if (notifikasiCount() > "0") {
                                $notifikasiList = notifikasiList();
                            ?>
                                <div class="list-group p-0">
                                    <?php foreach ($notifikasiList as $nl) : ?>
                                        <a href="<?= base_url("notifikasi/baca/" . $nl->notifikasi_id); ?>" class="border-left-0 border-top-0 border-right-0 border-primary list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex justify-content-between">
                                                <h6 class="mb-1"><?= $nl->judul; ?></h6>
                                                <small><?= time_elapsed_string($nl->create_at); ?></small>
                                            </div>
                                            <p class="mb-1 text-muted small"><?= $nl->pesan; ?></p>
                                        </a>
                                    <?php endforeach; ?>
                                    <a href="<?= base_url("notifikasi"); ?>" class="border-0 list-group-item list-group-item-action flex-column align-items-start">
                                        <p class="mb-1 text-muted small">Lihat Semua</p>
                                    </a>
                                </div>
                            <?php } else {
                            ?>
                                <p class="dropdown-item-text">Belum ada notifikasi.</p>
                            <?php
                            } ?>
                        </div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" title="Keranjang Belanja" href="<?= base_url('keranjang/saya'); ?>">
                        <i class="fas fa-shopping-cart fa-fw position-relative">
                            <span class="badge badge-danger position-absolute rounded-circle" id="badgeKeranjang"></span>
                        </i>
                        <span class="d-inlin-block d-lg-none"> Keranjang Belanja</span>
                    </a>
                </li>
                <?php if (!empty($this->session->userdata("penjual"))) { ?>
                    <li class="nav-item">
                        <a class="nav-link" title="Toko Saya" href="<?= base_url('toko/saya'); ?>">
                            <i class="fas fa-store fa-fw"></i>
                            <span class="d-inline-block d-lg-none"> Toko Saya</span>
                        </a>
                    </li>
                <?php } ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" title="Profil" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user fa-fw"></i>
                        <span class="d-inline-block d-lg-none">
                            <?= $this->session->userdata("user")["nama_lengkap"]; ?>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <div class="dropdown-item">
                            Hai, <?= user("nama_lengkap"); ?>
                        </div>
                        <div class="dropdown-item">
                            <small class="badge badge-primary">
                                Point Anda <?= rupiah(get_point_now()); ?>
                            </small>
                        </div>
                        <a class="dropdown-item" href="<?= base_url('profil'); ?>">Profil</a>
                        <a class="dropdown-item" href="<?= base_url('profil/pembelian'); ?>">Pembelian</a>
                        <a class="dropdown-item" href="<?= base_url('login/logout'); ?>">Keluar</a>
                    </div>
                </li>
            </ul>
        </div>
    <?php
    } else { ?>
        <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="text-primary"><i class="fas fa-bars"></i></span>
        </button>
        <div class="collapse navbar-collapse text-right py-2" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="btn btn-primary btn-sm mr-1" href="<?= base_url('login'); ?>">Masuk</a>
                    <a class="btn btn-outline-primary btn-sm" href="<?= base_url('register'); ?>">Daftar</a>
                </li>
            </ul>
        </div>
    <?php } ?>
</nav>