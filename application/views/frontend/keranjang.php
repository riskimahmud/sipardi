<div class="container mt-3">
    <div class="row">
        <?php if (!$keranjang) { ?>
            <div class="col-md-12 alert alert-info"><i class="fas fa-info-circle fa-fw"></i> Keranjang belanja masih kosong.</div>
        <?php } else { ?>
            <div class="col-md-7 px-2" id="keranjang">
                <div class="alert alert-info align-top">
                    <i class="fas fa-info-circle fa-fw"></i>
                    Keranjang belanja hanya dapat diisi dari 1 toko.
                </div>
                <a href="<?= base_url("penjual/" . $penjual->id_penjual . "/" . $penjual->penjual_seo); ?>" class="btn btn-primary btn-sm mb-2">
                    Belanja Lagi.
                </a>
                <?php
                foreach ($keranjang as $k) :
                    $produk = ambil_data_by_id_row("produk", "id_produk", $k->id_produk);
                    $stok = ambil_data_sum_by_id_row("stok", "stok", ["id_produk" => $produk->id_produk]);
                    $terjual = ambil_data_sum_by_id_row("rb_penjualan_detail", "qty", ["id_produk" => $produk->id_produk, "status >" => "0"], ["table" => "rb_penjualan", "key" => "id_penjualan"]);
                    $stok = $stok->stok - $terjual->qty;
                ?>
                    <div class="card mb-2 p-2">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-6" style="height: 120px; width: 100%; overflow: hidden;">
                                <img src="<?= base_url('uploads/produk/' . $produk->gambar); ?>" alt="gambar" class="img-fluid">
                            </div>
                            <div class="col-md-9 col-sm-6 col-6">
                                <b>
                                    <?= $produk->nama_produk; ?>
                                </b>

                                <div class="d-block harga" data-harga="<?= $k->harga; ?>">
                                    <?php
                                    if ($k->diskon > 0) {
                                        echo "<s>" . rupiah($k->harga_asli) . "</s>&nbsp;" . rupiah($k->harga);
                                    } else {
                                        echo rupiah($k->harga);
                                    }
                                    ?>
                                </div>
                                <div class="text-muted">
                                    <button type="button" class="btn btn-link btn-sm kurang"><i class="fas fa-minus-circle fa-sw text-secondary"></i></button>
                                    <input type="hidden" class="form-control" id="stok" value="<?= $stok; ?>">
                                    <span class="d-none"><?= $k->id_keranjang; ?></span>
                                    x<span id="qty"><?= $k->qty; ?></span>
                                    <button type="button" class="btn btn-link btn-sm tambah"><i class="fas fa-plus-circle fa-sw text-success"></i></button>
                                </div>
                                <div class="text-primary font-weight-bolder">
                                    <span class="total" data-total="<?= $k->total; ?>"><?= rupiah($k->total); ?></span>
                                    <div class="float-right">
                                        <div class="btn-group-sm">
                                            <!-- <a class="btn btn-sm btn-secondary edit" href="#"><i class="fas fa-edit fa-sw"></i></a> -->
                                            <a class="btn btn-sm btn-danger hapus" href="<?= base_url(); ?>" id="<?= $k->id_keranjang; ?>">
                                                <i class="fas fa-trash fa-sw"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-md-5 px-2 box-checkout">
                <?php if ($keranjang) { ?>
                    <?= form_open("keranjang/checkout") ?>
                    <label for="keterangan">Keterangan Pembeli</label>
                    <textarea name="keterangan" rows="2" id="keterangan" class="form-control mb-3" placeholder="Contoh (Masukkan kedalam tas plastik)" autofocus></textarea>

                    <div class="card">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <b class="d-block" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Alamat Pengantaran : <br><small>Otomatis diambil dari alamat anda (klik untuk mengubah)</small>
                                </b>
                                <div class="collapse border-top" id="collapseExample">
                                    <div class="form-group mt-3">
                                        <label for="alamat">Alamat</label>
                                        <textarea name="alamat" id="alamat" rows="2" class="form-control form-control-sm"><?= $pembeli->alamat_lengkap; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="kabkota">Kab / Kota</label>
                                        <select class="kota-pembeli form-control form-control-sm<?= (form_error('kota_id')) ? 'is-invalid' : ''; ?>" id="kabkota" name="kota_id">
                                            <option selected disabled>Pilih Kab / Kota</option>
                                            <?php foreach ($kota as $k) { ?>
                                                <option value="<?= $k->kota_id; ?>" <?= ($k->kota_id == $pembeli->kota_id) ? 'selected' : ''; ?>>
                                                    <?= $k->nama_kota; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <?php echo form_error('kota_id'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="kecamatan">Kecamatan</label>
                                        <select class="kecamatan-pembeli form-control form-control-sm <?= (form_error('kecamatan')) ? 'is-invalid' : ''; ?>" id="kecamatan" name="kecamatan">
                                            <option selected disabled>Pilih Kecamatan</option>
                                            <?php
                                            if ($pembeli->kota_id != "0") {
                                                $kecamatan = ambil_data_by_id('rb_kecamatan', 'kota_id', $pembeli->kota_id);
                                                foreach ($kecamatan as $kec) :
                                            ?>
                                                    <option value="<?= $kec->kecamatan_id; ?>" <?= ($kec->kecamatan_id == $pembeli->kecamatan) ? 'selected' : ''; ?>>
                                                        <?= $kec->nama_kecamatan ?>
                                                    </option>
                                            <?php
                                                endforeach;
                                            }
                                            ?>
                                            <!--<option value="<?= $pembeli->kecamatan; ?>" selected>-->
                                            <!--    <?= ambil_nama_by_id("rb_kecamatan", "nama_kecamatan", "kecamatan_id", $pembeli->kecamatan); ?>-->
                                            <!--</option>-->
                                        </select>
                                        <?php echo form_error('kecamatan'); ?>
                                    </div>
                                    <input type="hidden" class="id-penjual" value="<?= $penjual->id_penjual; ?>">
                                    <input type="hidden" class="kelurahan-penjual" value="<?= $penjual->kelurahan; ?>">
                                    <div class="form-group">
                                        <label for="kelurahan">Kelurahan</label>
                                        <select class="kelurahan-pembeli form-control form-control-sm <?= (form_error('kelurahan')) ? 'is-invalid' : ''; ?>" id="kelurahan" name="kelurahan">
                                            <option selected disabled>Pilih Kelurahan</option>
                                            <?php
                                            if ($pembeli->kota_id != "0") {
                                                $kelurahan = ambil_data_by_id('rb_desa', 'kecamatan_id', $pembeli->kecamatan);
                                                foreach ($kelurahan as $kel) :
                                            ?>
                                                    <option value="<?= $kel->desa_id; ?>" <?= ($kel->desa_id == $pembeli->kelurahan) ? 'selected' : ''; ?>>
                                                        <?= $kel->nama_desa ?>
                                                    </option>
                                            <?php
                                                endforeach;
                                            }
                                            ?>
                                            <!--<option value="<?= $pembeli->kelurahan; ?>" selected>-->
                                            <!--    <?= ambil_nama_by_id("rb_desa", "nama_desa", "desa_id", $pembeli->kelurahan); ?>-->
                                            <!--</option>-->
                                        </select>
                                        <?php echo form_error('kelurahan'); ?>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                Ongkos Pengantaran : <h6 class="ongkir" id="ongkir" data-ongkir=""></h6>
                            </li>
                            <li class="list-group-item">
                                <input type="text" name="kode_promo" id="kodePromo" class="form-control" placeholder="Kode Promo...">
                            </li>
                            <li class="list-group-item" id="keteranganPromo" style="display: none;">
                            </li>
                            <!--<li class="list-group-item">-->
                            <!--    <input type="text" name="kode_promo" class="form-control" placeholder="Kode Promo..." readonly>-->
                            <!--</li>-->
                        </ul>
                    </div>
                    <div class="card bg-light shadow-sm p-2 mb-2">
                        Total Bayar : <h4 class="d-inline font-weight-bold" id="totalBayar"></h4>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block my-2" id="checkout"><i class="fas fa-truck"></i> Checkout</button>
                    <?php form_close(); ?>
                <?php } ?>
            </div>
            <?php if ($keranjang) { ?>
                <div class="col-md-12 mt-3">
                    <!-- data penjual -->
                    <h5 class="font-weight-bold text-primary">
                        <a href="<?= base_url($penjual->penjual_seo); ?>">
                            Produk Lainnya Dari <?= $penjual->nama_penjual; ?>
                        </a>
                    </h5>
                    <div class="col produk-penjual m-0 p-0" id="container-produk">
                        <div class="row row-cols-2 row-cols-sm-12 row-cols-md-4 row-cols-lg-6" id="container-produk">
                            <?php foreach ($produk_penjual as $p) : ?>
                                <div class="col mb-3" id="product">
                                    <a href="<?= base_url($penjual->penjual_seo . '/' . $p->produk_seo); ?>" class="text-decoration-none">
                                        <div class="card h-100 product rounded">
                                            <div class="thumbnail">
                                                <img src="<?= base_url('uploads/produk/' . $p->gambar); ?>" class="card-img-top rounded-top" height="200px">
                                            </div>
                                            <div class="card-body">
                                                <div class="card-text">
                                                    <b class="text-truncate text-dark d-block"><?= $p->nama_produk; ?></b>
                                                    <h6 class="text-primary font-weight-bolder"><?= rupiah($p->harga_konsumen); ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <?php } ?>
    </div>
</div>
</div>