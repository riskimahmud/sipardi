<form action="<?= base_url('keranjang/index/' . $produk->id_produk); ?>" method="POST">
    <div class="container my-4">
        <div class="row">
            <?php
            if ($penjual->buka >= date("H:i") || $penjual->tutup <= date("H:i")) {
            ?>
                <div class="col-md-12 col-sm-12 mb-3 text-center">
                    <div class="alert alert-danger">
                        <i class="fas fa-info-circle"></i> Toko Sedang Tutup
                        <small class="d-block">Waktu Operasional 07:00 s/d 09:20</small>
                    </div>
                </div>
            <?php
            }
            ?>
            <?php if ($penjual->id_penjual == penjual("id_penjual")) {
                echo '
                    <div class="col-md-12 col-sm-12 mb-3 text-center">
                    <div class="alert alert-info"><i class="fas fa-info-circle"></i> Barang ini adalah barang di toko anda.</div>
                    </div>
                ';
            } ?>
            <div class="col-md-5 col-sm-12 mb-3 text-center">
                <?php if ($stok == "0") { ?><h1 class="alert alert-danger">Habis</h1><?php } ?>
                <?php if ($produk->diskon > 0) { ?><h1 class="alert alert-success my-0">Potongan <?= $produk->diskon . '%'; ?></h1><?php } ?>
                <div class="thumbnail-detail">
                    <img src="<?= base_url('uploads/produk/' . $produk->gambar); ?>" alt="Gambar produk" class="img-fluid w-100">
                </div>
                <!--<img src="<?= base_url('uploads/produk/' . $produk->gambar); ?>" alt="Gambar produk" class="img-fluid w-100">-->
            </div>
            <div class="col-md-7 col-sm-12">
                <h4 class="font-weight-bold"><?= $produk->nama_produk; ?></h4>
                <span>
                    <ul class="list-inline">
                        <li class="list-inline-item">Belum Ada Rating</li>
                        <li class="list-inline-item"><i class="text-primary fas fa-circle fa-xs"></i></li>
                        <li class="list-inline-item"><?= ($terjual) ? $terjual : 'Belum Ada Yang'; ?> Terjual</li>
                        <li class="list-inline-item"><i class="text-primary fas fa-circle fa-xs"></i></li>
                        <li class="list-inline-item"><?= $produk->hits . 'x Dilihat'; ?></li>
                    </ul>
                </span>
                <hr class="dropdown-divider">

                <div class="row mt-4">
                    <div class="col-md-2 col-sm-6">
                        <h5 class="text-muted font-weight-light">Harga</h5>
                    </div>
                    <div class="col-md-10 col-sm-12">
                        <h3 class="text-primary font-weight-bolder d-inline-block">
                            <?php if ($produk->diskon == 0) : ?>
                                <span id="hargaLabel"><?= rupiah($produk->harga_konsumen); ?></span>
                            <?php
                            else :
                                $new_harga = $produk->harga_konsumen - (($produk->diskon / 100) * $produk->harga_konsumen);
                            ?>
                                <s class="text-muted small"><?= rupiah($produk->harga_konsumen); ?></s>
                                <span id="hargaLabel"><?= rupiah($new_harga); ?></span>
                            <?php endif; ?>
                            <small>/ <?= $produk->satuan; ?></small>
                        </h3>
                        <?php if ($produk->harga_nego > 0 && $produk->diskon == 0) { ?>
                            <span class="d-inline-block float-right">
                                <a href="#" class="btn btn-primary btn-sm" id="tawar" data-idproduk="<?= $produk->id_produk; ?>">Boleh Tawar</a>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="col-sm-12">
                        <hr class="dropdown-divider">
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <h5 class="text-muted font-weight-light">Stok</h5>
                    </div>
                    <div class="col-md-10 col-sm-12">
                        <h3 class="font-weight-light"><?= $stok . " " . $produk->satuan; ?></h3>
                    </div>
                    <div class="col-sm-12">
                        <hr class="dropdown-divider">
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <h5 class="text-muted font-weight-light">Jumlah Beli</h5>
                    </div>
                    <div class="col-md-10 col-sm-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-6 input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-secondary" type="button" id="kurangJumlahBeli"><i class="fas fa-minus-circle"></i></button>
                                </div>
                                <input type="hidden" class="form-control" name="id_produk" id="idProduk" value="<?= $produk->id_produk; ?>">
                                <input type="hidden" class="form-control" name="id_penjual" value="<?= $produk->id_penjual; ?>">
                                <input type="hidden" class="form-control" name="diskon" value="<?= $produk->diskon; ?>">
                                <input type="hidden" class="form-control" name="total" id="total" value="">
                                <input type="hidden" class="form-control" name="harga" id="harga" value="<?= ($produk->diskon > 0) ? $new_harga : $produk->harga_konsumen; ?>">
                                <input type="hidden" class="form-control" name="harga_asli" value="<?= $produk->harga_konsumen ?>">
                                <input type="hidden" class="form-control" id="stok" value="<?= $stok; ?>">
                                <input type="text" class="form-control" id="jumlahBeli" name="qty" value="1" onkeypress="hanyaAngka(event);" autofocus>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" id="tambahJumlahBeli"><i class="fas fa-plus-circle"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <hr class="dropdown-divider">
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <h5 class="text-muted font-weight-light">Info Produk</h5>
                    </div>
                    <div class="col-md-10 col-sm-12">
                        <div class="">
                            <ul class="list-inline">
                                <li class="list-inline-item border-right border-primary pr-4">
                                    <dd>Berat</dd>
                                    <dt><?= $produk->berat . ' gram'; ?></dt>
                                </li>
                                <li class="list-inline-item">
                                    <dd>Kondisi</dd>
                                    <dt><?= ($produk->kondisi) ? $produk->kondisi : '-'; ?></dt>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <hr class="dropdown-divider">
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-12 bg-light shadow p-2">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="pills-deskripsi-tab" data-toggle="pill" href="#pills-deskripsi" role="tab" aria-controls="pills-deskripsi" aria-selected="true">Deskripsi</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-ulasan-tab" data-toggle="pill" href="#pills-ulasan" role="tab" aria-controls="pills-ulasan" aria-selected="false">Ulasan</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-diskusi-tab" data-toggle="pill" href="#pills-diskusi" role="tab" aria-controls="pills-diskusi" aria-selected="false">Diskusi</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent px-3">
                            <div class="tab-pane fade show active" id="pills-deskripsi" role="tabpanel" aria-labelledby="pills-deskripsi-tab"><?= $produk->deskripsi; ?></div>
                            <div class="tab-pane fade" id="pills-ulasan" role="tabpanel" aria-labelledby="pills-ulasan-tab">
                                <div class="alert alert-info"><i class="fas fa-info-circle"></i> Belum ada ulasan.</div>
                            </div>
                            <div class="tab-pane fade" id="pills-diskusi" role="tabpanel" aria-labelledby="pills-diskusi-tab">
                                <?php if (!user("id_konsumen")) {
                                    echo '<div class="alert alert-info">Masuk terlebih dahulu. untuk mengirim pertanyaan.</div>';
                                } elseif ($penjual->id_penjual == penjual("id_penjual")) {
                                } else { ?>

                                    <p>
                                        <a class="btn btn-secondary btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            Kirim Pesan
                                        </a>
                                    </p>
                                    <div class="collapse mb-3 p-0" id="collapseExample">
                                        <div class="card card-body border-0 p-0">
                                            <div class="form-group">
                                                <textarea name="pesan" id="pesanDiskusi" class="form-control" rows="3" placeholder="Masukkan pertanyaan anda disini"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" id="kirimDiskusi" type="button"><i class="fas fa-paper-plane"></i> Kirim</button>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>

                                <?php
                                if (empty($diskusi)) {
                                    echo '<div class="alert alert-info"><i class="fas fa-info-circle"></i> Belum ada diskusi.</div>';
                                }
                                foreach ($diskusi as $dis) :
                                    $gambar = ambil_nama_by_id("users", "foto", "id_konsumen", $dis->pengirim);
                                    $nama = ambil_nama_by_id("users", "nama_lengkap", "id_konsumen", $dis->pengirim);
                                ?>
                                    <div class="media mb-3">
                                        <img src="<?= base_url("uploads/users/" . $gambar); ?>" class="align-self-start mr-3 foto-diskusi" alt="">
                                        <div class="media-body">
                                            <small><?= $nama; ?></small>
                                            <p><?= $dis->pesan; ?></p>
                                            <small class="text-muted"><?= time_elapsed_string($dis->create_at); ?></small>
                                            <?php if ($dis->status == "1") { ?>
                                                <div class="media mt-2">
                                                    <img src="<?= base_url("uploads/users/" . ambil_nama_by_id("penjual", "foto", "id_penjual", $penjual->id_penjual)); ?>" class="align-self-start mr-3 foto-diskusi" alt="">
                                                    <div class="media-body">
                                                        <small>Penjual</small>
                                                        <p><?= ambil_nama_by_id("diskusi", "pesan", "reply_for", $dis->id_diskusi); ?></p>
                                                        <small class="text-muted"><?= time_elapsed_string(ambil_nama_by_id("diskusi", "create_at", "reply_for", $dis->id_diskusi)); ?></small>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row row-cols-1 row-cols-md-2 fixed-bottom bg-white border-top border-secondary py-2">
                    <div class="col mb-2 text-center">
                        <b class="text-primary font-weight-bold d-block text-truncate">
                            <a href="<?= base_url($penjual->penjual_seo); ?>">
                                <?= ($penjual) ? $penjual->nama_penjual : ''; ?>
                            </a>
                            <?php if ($penjual->status_penjual == "1") { ?>
                                <i class="fas fa-check-double fa-fw" id="badgeSeller" data-toggle="tooltip" data-placement="top" title="Toko Terverifikasi"></i>
                            <?php } ?>
                        </b>
                        <?php if ($penjual->kelurahan != "0") { ?>
                            <small>
                                <?= ambil_nama_by_id("rb_desa", "nama_desa", "desa_id", $penjual->kelurahan); ?>,
                                <?= ambil_nama_by_id("rb_kecamatan", "nama_kecamatan", "kecamatan_id", $penjual->kecamatan); ?>,
                                <?= ambil_nama_by_id("rb_kota", "nama_kota", "kota_id", $penjual->kota_id); ?>
                            </small>
                        <?php } ?>
                        <!-- <a href="#" class="btn btn-primary float-right"><i class="fas fa-plus-square fa-fw"></i> Ikuti</a> -->
                    </div>
                    <div class="col">
                        <div class="row row-cols-2 row-cols-md-2 row-cols-lg-2 text-center">
                            <div class="col">
                                <dd class="m-auto">Total</dd>
                                <dt class="m-auto font-weight-bolder" id="totalHarga"></dt>
                            </div>
                            <div class="col">
                                <?php if ((penjual("id_penjual")) && $penjual->id_penjual == penjual("id_penjual")) {
                                    echo '';
                                } else {
                                    if ($penjual->buka <= date("H:i") && $penjual->tutup >= date("H:i")) {
                                ?>
                                        <button type="submit" class="btn btn-primary btn-lg" <?= ($stok === 0) ? 'disabled="disabled"' : ''; ?>><i class="fas fa-shopping-bag fa-fw"></i> Pesan Sekarang</button>
                                <?php
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>