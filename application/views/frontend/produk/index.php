<div class="container-fluid mt-4">
    <div class="row row-cols-2 px-2">
        <div class="col-12 col-md-3 col-lg-3 mb-5 h-50" id="filter">
            <div class="card shadow-lg rounded-lg mb-3">
                <div class="card-header bg-primary text-white">
                    Filter
                </div>
                <div class="card-body" id="box-filter">
                    <?= form_open(base_url("produk"), ["autocomplete" => "off", "method" => "get"]); ?>
                    <div class="form-group">
                        <label for="keyword">Keyword</label>
                        <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Keyword" value="<?= (cari("keyword")) ? cari("keyword") : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label for="kategori">Kategori</label>
                        <select class="kategoriProduk form-control" id="kategori" name="kategori" data-allow-clear="1">
                            <!-- <option selected value="">Tanpa Kategori</option> -->
                            <?php if (cari("kategori")) { ?>
                                <option selected value="<?= cari("kategori"); ?>"><?= get_kategori(cari("kategori")); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sortir">Sortir</label>
                        <select name="sortir" id="sortir" class="form-control">
                            <option value="">Tanpa Sortir</option>
                            <option value="asc" <?= (cari("order")["value"] == "asc") ? 'selected' : ''; ?>>Harga Termurah</option>
                            <option value="desc" <?= (cari("order")["value"] == "desc") ? 'selected' : ''; ?>>Harga Termahal</option>
                        </select>
                    </div>
                    <div class="btn-group btn-group-sm btn-block mt-3">
                        <input type="submit" value="Cari" name="cari" class="btn btn-primary">
                        <input type="submit" value="Hapus" name="hapus" class="btn btn-dark">
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>

            <ul class="nav nav-pills flex-column p-0">
                <li class="nav-item">
                    <a class="nav-link active" href="<?= base_url("produk"); ?>">
                        <div class="fas fa-briefcase fa-fw"></div> Produk
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url("penjual"); ?>"><i class="fas fa-store"></i> Penjual</a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-9 col-lg-9">
            <div class="row row-cols-1" id="produk-beranda">
                <!-- Marketing Icons Section -->
                <div class="col">
                    <div class="row row-cols-1">
                        <div class="col mb-5 p-0">
                            <?php
                            if (empty($produk)) {
                            ?>
                                <div class="alert alert-success" role="alert">
                                    <h4 class="alert-heading">Oooops..!</h4>
                                    <p>Tidak ada barang untuk ditampilkan.</p>
                                </div>
                            <?php
                            } else {
                            ?>
                                <div class="container-fluid">
                                    <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-5" id="container-produk">
                                        <?php
                                        foreach ($produk as $p) :
                                            $penjual_seo = ambil_nama_by_id("penjual", "penjual_seo", "id_penjual", $p->id_penjual);
                                        ?>
                                            <div class="col p-1 mb-3" id="product">
                                                <!-- <a href="<?= base_url('produk/' . $p->id_produk . '/' . $p->produk_seo); ?>" class="text-decoration-none"> -->
                                                <a href="<?= base_url($penjual_seo . '/' . $p->produk_seo); ?>" class="text-decoration-none">
                                                    <div class="card h-100 product rounded">
                                                        <?php
                                                        $cek_operasional = cek_operasional($p->id_penjual);
                                                        if ($cek_operasional) {
                                                            echo "<div class='ribbon ribbon-top-left'><span><i class='fas fa-store-slash'></i> tutup</span></div>";
                                                        }
                                                        ?>
                                                        <?php if ($p->diskon > 0) : ?>
                                                            <div class='ribbon-discon ribbon-discon-top-left'><span> <?= $p->diskon; ?>% OFF</span></div>
                                                        <?php endif; ?>
                                                        <!-- <div class="tutup">TUTUUP</div> -->
                                                        <div class="thumbnail">
                                                            <img src="<?= base_url('uploads/produk/' . $p->gambar); ?>" class="card-img-top rounded-top">
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="card-text">
                                                                <b class="text-truncate text-dark d-block"><?= $p->nama_produk; ?></b>
                                                                <small class="text-truncate d-block"><?= ambil_nama_by_id("penjual", "nama_penjual", "id_penjual", $p->id_penjual); ?></small>
                                                                <?php
                                                                if ($p->diskon > 0) :
                                                                    $new_harga = $p->harga_konsumen - (($p->diskon / 100) * $p->harga_konsumen);
                                                                ?>
                                                                    <s class="text-muted small"><?= rupiah($p->harga_konsumen); ?></s>
                                                                    <h6 class="text-primary font-weight-bolder"><?= rupiah($new_harga); ?></h6>
                                                                <?php else : ?>
                                                                    <h6 class="text-primary font-weight-bolder"><?= rupiah($p->harga_konsumen); ?></h6>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        <?php
                                        endforeach;
                                        ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col mb-3">
                            <!--Tampilkan pagination-->
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>