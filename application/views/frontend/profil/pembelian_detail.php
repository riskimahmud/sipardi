<?php
if ($data->status == "1") {
    $icon = "clock";
    $msg = "Menunggu";
    $badge = "secondary";
} elseif ($data->status == "2") {
    $icon = "box-open";
    $msg = "Sedang Dikemas";
    $badge = "warning";
} elseif ($data->status == "3") {
    $icon = "truck";
    $msg = "Sedang Diantar";
    $badge = "info";
} elseif ($data->status == "4") {
    $msg = "Selesai";
    $icon = "check-circle";
    $badge = "success";
} else {
    $msg = "Dibatalkan";
    $icon = "times-circle";
    $badge = "danger";
}
?>
<div class="row">
    <div class="col-12 mt-3">
        <span class="d-block clearfix">
            <a href="<?= base_url("profil/pembelian/" . $status); ?>" class="btn btn-dark btn-sm float-right d-inline-block">Kembali</a>
        </span>
        <hr class="">
    </div>

    <div class="col-12">
        <h1 class="alert alert-<?= $badge; ?>">
            <i class="fas fa-fw fa-<?= $icon; ?>"></i>
            <?= $msg; ?>
        </h1>

        <?php if ($data->status == "0") : ?>
            <ul class="list-group-flush p-0">
                <li class="list-group-item px-0 py-2 bg-transparent">
                    Keterangan Penjual :
                    <span class="d-block text-muted"><?= $data->keterangan_penjual; ?></span>
                </li>
            </ul>
        <?php endif; ?>
    </div>
    <div class="col-md-6 col-sm-6 mb-3">
        <ul class="list-group-flush p-0">
            <li class="list-group-item px-0 py-2 bg-transparent">
                Kode Transaksi : <?= $data->kode_transaksi; ?>
                <small class="d-block">
                    <?= time_elapsed_string($data->waktu_transaksi); ?>
                </small>
            </li>
            <li class="list-group-item px-0 py-2 bg-transparent">
                Kode Promo : <?= $data->kode_promo; ?>
                <?php if ($data->kode_promo != "") : ?>
                    <small class="d-block">
                        <?= ambil_nama_by_id('promo', 'keterangan', 'kode_referal', $data->kode_promo); ?>
                    </small>
                <?php endif; ?>
            </li>
            <?php
            foreach ($detail as $d) :
                $produk = ambil_datafield_by_id_row("produk", "nama_produk, gambar, satuan", ["id_produk" => $d->id_produk]);
            ?>
                <li class="list-group-item px-0 py-1 bg-transparent">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-6">
                            <img src="<?= base_url('uploads/produk/' . $produk->gambar); ?>" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                            <b class="font-weight-bold text-truncate d-block"><?= $produk->nama_produk; ?></b>
                            <ul class="list-unstyled">
                                <li><?= $d->qty . " " . $produk->satuan; ?></li>
                                <li>
                                    <?php
                                    if ($d->diskon > 0) {
                                        echo "<del>" . rupiah($d->harga_asli) . "</del><br>" . rupiah($d->harga) . "<br><span class='badge badge-success'>OFF $d->diskon%</span>";
                                    } else {
                                        rupiah($d->harga);
                                    }
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            <?php endforeach ?>
            <!--<li class="list-group-item px-0 py-2 bg-transparent text-right">-->
            <!--    Ongkos Antar : <?= rupiah($data->ongkir); ?>-->
            <!--</li>-->
            <!--<li class="list-group-item px-0 py-2 bg-transparent text-right">-->
            <!--    <h5>Total Bayar : <span class="font-weight-bold"><?= rupiah($data->total_produk + $data->ongkir); ?></span></h5>-->
            <!--</li>-->
            <li class="list-group-item px-0 py-2 bg-transparent text-right">
                <!--Sub Total : <?= rupiah($data->total_produk); ?>-->
                <?php if ($data->potongan_barang > 0) : ?>
                    Sub Total :
                    <del class="mr-1">
                        <?= rupiah($data->total_produk); ?>
                    </del>
                    <?= rupiah($data->total_produk - $data->potongan_barang); ?>
                <?php else : ?>
                    Sub Total : <?= rupiah($data->total_produk); ?>
                <?php endif; ?>
            </li>
            <li class="list-group-item px-0 py-2 bg-transparent text-right">
                <?php if ($data->potongan_ongkir > 0) : ?>
                    Ongkos Antar :
                    <del class="mr-1">
                        <?= rupiah($data->ongkir); ?>
                    </del>
                    <?= rupiah($data->ongkir - $data->potongan_ongkir); ?>
                <?php else : ?>
                    Ongkos Antar : <?= rupiah($data->ongkir); ?>
                <?php endif; ?>
            </li>
            <li class="list-group-item px-0 py-2 bg-transparent text-right">
                <h5>Total Bayar :
                    <span class="font-weight-bold">
                        <?php if ($data->kode_promo != "") : ?>
                            <?= rupiah($data->total_produk + $data->ongkir - $data->potongan_ongkir - $data->potongan_barang); ?>
                        <?php else : ?>
                            <?= rupiah($data->total_produk + $data->ongkir); ?>
                        <?php endif; ?>
                    </span>
                </h5>
            </li>
            <li class="list-group-item px-0 py-2 bg-transparent">
                Keterangan Pembeli :
                <span class="d-block text-muted"><?= $data->keterangan_pembeli; ?></span>
            </li>
            <li class="list-group-item px-0 py-2 bg-transparent">
                Lokasi Pengantaran :
                <span class="d-block text-muted"><?= $data->alamat_pembeli; ?></span>
                <span class="d-block text-muted">
                    <?= $lokasi_antar->nama_desa . ", " . get_kecamatan($lokasi_antar->kecamatan_id) . ", " . get_kota($lokasi_antar->kota_id); ?>
                </span>
            </li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6">
        <b>Penjual</b>
        <hr>
        <ul class="list-group-flush p-0">
            <li class="list-group-item p-0 mb-3 bg-transparent">
                <b><?= $penjual->nama_penjual; ?></b>
            </li>
            <li class="list-group-item p-0 mb-3 bg-transparent">
                <?= $penjual->alamat_lengkap; ?>
            </li>
            <li class="list-group-item p-0 mb-3 bg-transparent">
                <?= get_kelurahan($penjual->kelurahan) . ', ' . get_kecamatan($penjual->kecamatan) . ', ' . get_kota($penjual->kota_id); ?>
            </li>
            <li class="list-group-item p-0 mb-3 bg-transparent">
                <?= $penjual->no_telpon; ?>
            </li>
        </ul>
    </div>
    <?php if (!empty($riwayat)) : ?>
        <div class="col-md-12">
            <hr>
            <h4>Tracing Pesanan</h4>
            <ul class="timeline">
                <?php foreach ($riwayat as $riw) : ?>
                    <li>
                        <span><?= strtoupper(status_num_to_string($riw->status)) ?></span>
                        <span class="float-right">
                            <?php
                            $tanggal = substr($riw->create_at, 0, 10);
                            if ($tanggal == date("Y-m-d")) {
                                echo time_elapsed_string($riw->create_at);
                            } else {
                                echo tgl_full($riw->create_at);
                            }
                            ?>
                        </span>
                        <p><?= $riw->keterangan; ?></p>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
</div>