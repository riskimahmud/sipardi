<div class="row p-2">
    <div class="col-md-12">
        <?php echo form_open('', 'method="POST" autocomplete="off" enctype="multipart/form-data"');
        ?>
        <!-- <form action="" method="POST" autocomplete="off"> -->
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <div class="form-group row mb-4">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" value="<?= $data->email; ?>" readonly aria-describedby="emailHelpBlock">
                <small id="emailHelpBlock" class="form-text text-muted">
                    Email tidak dapat diganti
                </small>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="nama" class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('nama_lengkap')) ? 'is-invalid' : ''; ?>" id="nama_lengkap" name="nama_lengkap" value="<?= set_value("nama_lengkap", $data->nama_lengkap, false) ?>" autofocus>
                <?php echo form_error('nama_lengkap'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="no_hp" class="col-sm-2 col-form-label">No. HP</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('no_hp')) ? 'is-invalid' : ''; ?>" id="no_hp" name="no_hp" value="<?= set_value("no_hp", $data->no_hp, false) ?>">
                <?php echo form_error('no_hp'); ?>
            </div>
        </div>
        <fieldset class="form-group mb-4">
            <div class="row">
                <legend class="col-form-label col-sm-2 pt-0">Jenis Kelamin</legend>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="gridRadios1" value="Laki-laki" <?= ($data->jenis_kelamin == "Laki-laki" || $data->jenis_kelamin == "") ? 'checked' : ''; ?> <?= set_radio("jenis_kelamin", "Laki-laki"); ?>>
                        <label class="form-check-label" for="gridRadios1">
                            Laki-laki
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="gridRadios2" value="Perempuan" <?= ($data->jenis_kelamin == "Perempuan") ? 'checked' : ''; ?> <?= set_radio("jenis_kelamin", "Perempuan"); ?>>
                        <label class="form-check-label" for="gridRadios2">
                            Perempuan
                        </label>
                    </div>
                    <?php echo form_error('jenis_kelamin'); ?>
                </div>
            </div>
        </fieldset>
        <div class="form-group row mb-4">
            <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('tempat_lahir')) ? 'is-invalid' : ''; ?>" id="tempat_lahir" name="tempat_lahir" value="<?= set_value("tempat_lahir", $data->tempat_lahir, false) ?>">
                <?php echo form_error('tempat_lahir'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="tanggal_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('tanggal_lahir')) ? 'is-invalid' : ''; ?> datepicker" id="tanggal_lahir" name="tanggal_lahir" value="<?= set_value("tanggal_lahir", $data->tanggal_lahir, false) ?>">
                <?php echo form_error('tanggal_lahir'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="alamat_lengkap" class="col-sm-2 col-form-label">Alamat Lengkap</label>
            <div class="col-sm-10">
                <textarea name="alamat_lengkap" id="alamat_lengkap" rows="2" class="form-control <?= (form_error('alamat_lengkap')) ? 'is-invalid' : ''; ?>"><?= set_value("alamat_lengkap", $data->alamat_lengkap, false); ?></textarea>
                <?php echo form_error('alamat_lengkap'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="" class="col-sm-2 col-form-label"></label>
            <div class="col-md-10">
                <div class="form-row">
                    <div class="col">
                        <select class="form-control <?= (form_error('kota_id')) ? 'is-invalid' : ''; ?>" id="kabkota" name="kota_id">
                            <option selected disabled>Pilih Kab / Kota</option>
                            <?php foreach ($kota as $k) { ?>
                                <option value="<?= $k->kota_id; ?>" <?= ($k->kota_id == $data->kota_id) ? 'selected' : ''; ?>>
                                    <?= $k->nama_kota; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('kota_id'); ?>
                    </div>
                    <div class="col">
                        <select class="form-control <?= (form_error('kecamatan')) ? 'is-invalid' : ''; ?>" id="kecamatan" name="kecamatan">
                            <option selected disabled>Pilih Kecamatan</option>
                            <?php if ($data->kecamatan != "0") { ?>
                                <?php
                                $kecamatan = ambil_data_by_id('rb_kecamatan', 'kota_id', $data->kota_id);
                                foreach ($kecamatan as $kec) :
                                ?>
                                    <option value="<?= $kec->kecamatan_id; ?>" <?= ($kec->kecamatan_id == $data->kecamatan) ? 'selected' : ''; ?>>
                                        <?= $kec->nama_kecamatan ?>
                                    </option>
                                <?php
                                endforeach;
                                ?>
                                <!--<option value="<?= $data->kecamatan; ?>" selected>-->
                                <!--    <?= ambil_nama_by_id("rb_kecamatan", "nama_kecamatan", "kecamatan_id", $data->kecamatan); ?>-->
                                <!--</option>-->
                            <?php } ?>
                        </select>
                        <?php echo form_error('kecamatan'); ?>
                    </div>
                    <div class="col">
                        <select class="form-control <?= (form_error('kelurahan')) ? 'is-invalid' : ''; ?>" id="kelurahan" name="kelurahan">
                            <option selected disabled>Pilih Kelurahan</option>
                            <?php if ($data->kelurahan != "0") { ?>
                                <?php
                                $kelurahan = ambil_data_by_id('rb_desa', 'kecamatan_id', $data->kecamatan);
                                foreach ($kelurahan as $kel) :
                                ?>
                                    <option value="<?= $kel->desa_id; ?>" <?= ($kel->desa_id == $data->kelurahan) ? 'selected' : ''; ?>>
                                        <?= $kel->nama_desa; ?>
                                    </option>
                                <?php
                                endforeach;
                                ?>
                                <!--<option value="<?= $data->kelurahan; ?>" selected>-->
                                <!--    <?= ambil_nama_by_id("rb_desa", "nama_desa", "desa_id", $data->kelurahan); ?>-->
                                <!--</option>-->
                            <?php } ?>
                        </select>
                        <?php echo form_error('kelurahan'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="alamat_lengkap" class="col-sm-2 col-form-label">Foto</label>
            <div class="col-sm-2">
                <img src="<?= base_url("uploads/users/" . $data->foto); ?>" class="img-thumbnail img-preview">
            </div>
            <div class="col-sm-8">
                <div class="custom-file">
                    <input type="hidden" name="foto_lama" value="<?= $data->foto; ?>">
                    <input type="file" class="custom-file-input form-control <?= (form_error('foto')) ? 'is-invalid' : ''; ?>" id="foto" name="foto" onchange="previewImg()" aria-describedby="fotoHelpBlock">
                    <label class="custom-file-label" for="foto">
                        <?= ($data->foto == 'default.jpg' || $data->foto == "") ? 'Pilih Gambar' : $data->foto; ?>
                    </label>
                    <small id="fotoHelpBlock" class="form-text text-muted">
                        Kosongkan jika tidak ingin mengganti foto.
                    </small>
                    <?php echo form_error('foto'); ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10 col-12">
                <button type="submit" name="simpan" class="btn btn-primary" value="simpan">Simpan</button>
                <button type="reset" class="btn btn-danger">Reset</button>
                <a href="<?= base_url('profil'); ?>" class="btn btn-dark">Kembali</a>
            </div>
        </div>
        <?= form_close(); ?>
        <!-- </form> -->
    </div>
</div>