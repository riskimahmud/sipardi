<div class="row p-2">
    <div class="col-md-4 col-12 mb-3">
        <img src="<?= base_url('uploads/users/' . $profil->foto); ?>" class="img-thumbnail rounded-lg">
        <a href="<?= base_url('profil/edit'); ?>" class="btn btn-outline-primary btn-block mt-2"><i class="fas fa-edit fa-fw"></i> Edit Profil</a>
        <a href="<?= base_url('profil/ganti_katasandi'); ?>" class="btn btn-outline-danger btn-block mt-2"><i class="fas fa-user-lock fa-fw"></i> Ganti Password</a>
    </div>
    <div class="col-md-8 col-12">
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Nama</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= $profil->nama_lengkap; ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Email</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= $profil->email; ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">No Handphone</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= $profil->no_hp; ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Tanggal Lahir</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= $profil->tanggal_lahir; ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Jenis Kelamin</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= $profil->jenis_kelamin; ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Alamat</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= $profil->alamat_lengkap; ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Kel / Desa</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= get_kelurahan($profil->kelurahan); ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Kecamatan</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= get_kecamatan($profil->kecamatan); ?></div>
        </div>
        <div class="row  mb-2">
            <div class="col-sm-4 col-12">Kab / Kota</div>
            <div class="col-sm-8 col-12 font-weight-bold"><?= get_kota($profil->kota_id); ?></div>
        </div>
    </div>
</div>