<div class="row p-2">
    <div class="col-md-12">
        <?php echo form_open('', 'method="POST" autocomplete="off" enctype="multipart/form-data"');
        ?>
        <div class="form-group row mb-4">
            <label for="nama_penjual" class="col-sm-2 col-form-label">Nama Toko / Usaha</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('nama_penjual')) ? 'is-invalid' : ''; ?>" id="nama_penjual" name="nama_penjual" value="<?= set_value("nama_penjual", '', false) ?>" autofocus>
                <?php echo form_error('nama_penjual'); ?>

                <div class="form-check mt-3">
                    <input class="form-check-input <?= (form_error('pasar')) ? 'is-invalid' : ''; ?>" type="checkbox" name="klaster_pasar" value="1" id="penjualPasar">
                    <label class="form-check-label" for="penjualPasar">
                        Penjual Di Pasar?
                    </label>
                    <?= form_error("pasar"); ?>
                </div>

            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="no_hp" class="col-sm-2 col-form-label">No. HP</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('no_telpon')) ? 'is-invalid' : ''; ?>" id="no_telpon" name="no_telpon" value="<?= set_value("no_telpon", '', false) ?>">
                <?php echo form_error('no_telpon'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="alamat_lengkap" class="col-sm-2 col-form-label">Alamat Lengkap</label>
            <div class="col-sm-10">
                <textarea name="alamat_lengkap" id="alamat_lengkap" rows="2" class="form-control <?= (form_error('alamat_lengkap')) ? 'is-invalid' : ''; ?>"><?= set_value("alamat_lengkap", '', false); ?></textarea>
                <?php echo form_error('alamat_lengkap'); ?>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="" class="col-sm-2 col-form-label"></label>
            <div class="col-md-10">
                <div class="form-row">
                    <div class="col">
                        <select class="form-control <?= (form_error('kota_id')) ? 'is-invalid' : ''; ?>" id="kabkota" name="kota_id">
                            <option selected disabled>Pilih Kab / Kota</option>
                            <?php foreach ($kota as $k) { ?>
                                <option value="<?= $k->kota_id; ?>">
                                    <?= $k->nama_kota; ?>
                                </option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('kota_id'); ?>
                    </div>
                    <div class="col">
                        <select class="form-control <?= (form_error('kecamatan')) ? 'is-invalid' : ''; ?>" id="kecamatan" name="kecamatan">
                            <option selected disabled>Pilih Kecamatan</option>
                        </select>
                        <?php echo form_error('kecamatan'); ?>
                    </div>
                    <div class="col">
                        <select class="form-control <?= (form_error('kelurahan')) ? 'is-invalid' : ''; ?>" id="kelurahan" name="kelurahan">
                            <option selected disabled>Pilih Kelurahan</option>
                        </select>
                        <?php echo form_error('kelurahan'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label for="alamat_lengkap" class="col-sm-2 col-form-label">Foto</label>
            <div class="col-sm-2">
                <img src="<?= base_url("uploads/users/default.jpg"); ?>" class="img-thumbnail img-preview">
            </div>
            <div class="col-sm-8">
                <div class="custom-file">
                    <input type="file" class="custom-file-input form-control <?= (form_error('foto')) ? 'is-invalid' : ''; ?>" id="foto" name="foto" onchange="previewImg()" aria-describedby="fotoHelpBlock">
                    <label class="custom-file-label" for="foto">
                        Pilih Gambar
                    </label>
                    <small id="fotoHelpBlock" class="form-text text-muted">
                        Format JPG|PNG Maksimal 2Mb
                    </small>
                    <?php echo form_error('foto'); ?>
                </div>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label class="col-sm-2 col-form-label">KTP</label>
            <div class="col-sm-2">
                <img src="<?= base_url("uploads/users/default.jpg"); ?>" class="img-thumbnail ktp-preview">
            </div>
            <div class="col-sm-8">
                <div class="custom-file">
                    <input type="file" class="custom-file-input form-control <?= (form_error('ktp')) ? 'is-invalid' : ''; ?>" id="ktp" name="ktp" onchange="previewKtp()" aria-describedby="ktpHelpBlock">
                    <label class="custom-file-label ktp-label" for="ktp">
                        Pilih KTP
                    </label>
                    <small id="ktpHelpBlock" class="form-text text-muted">
                        Format JPG|PNG Maksimal 2Mb
                    </small>
                    <?php echo form_error('ktp'); ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <button type="submit" name="simpan" class="btn btn-primary btn-block" value="simpan">Daftar</button>
            </div>
        </div>
        <?= form_close(); ?>
        <!-- </form> -->

        <small class="d-block text-muted text-center ">Dengan mendaftar, saya menyetujui</small>
        <small class="d-block text-muted text-center"><a href="#">Syarat dan ketentuan</a> serta <a href="#">Kebijakan privasi</a></small>
    </div>
</div>