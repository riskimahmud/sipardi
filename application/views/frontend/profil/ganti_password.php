<div class="row p-2">
    <div class="col-md-12">
        <?php echo form_open('', 'method="POST" autocomplete="off" enctype="multipart/form-data"');
        ?>
        <!-- <form action="" method="POST" autocomplete="off"> -->
        <div class="form-group row mb-3">
            <label for="pass_lama" class="col-sm-3 col-form-label">Password Lama</label>
            <div class="col-sm-9">
                <input type="password" class="form-control <?= (form_error('pass_lama')) ? 'is-invalid' : ''; ?>" id="pass_lama" name="pass_lama" value="<?= set_value("pass_lama", '', false) ?>" autofocus>
                <span toggle="#pass_lama" class="text-primary fas fa-eye field-icon toggle-password"></span>
                <?php echo form_error('pass_lama'); ?>
            </div>
        </div>
        <div class="form-group row mb-3">
            <label for="pass_baru" class="col-sm-3 col-form-label">Password Baru</label>
            <div class="col-sm-9">
                <input type="password" class="form-control <?= (form_error('pass_baru')) ? 'is-invalid' : ''; ?>" id="pass_baru" name="pass_baru" value="<?= set_value("pass_baru", '', false) ?>" autofocus>
                <span toggle="#pass_baru" class="text-primary fas fa-eye field-icon toggle-password"></span>
                <?php echo form_error('pass_baru'); ?>
            </div>
        </div>
        <div class="form-group row mb-3">
            <label for="pass_baru2" class="col-sm-3 col-form-label">Ulangi Password Baru</label>
            <div class="col-sm-9">
                <input type="password" class="form-control <?= (form_error('pass_baru2')) ? 'is-invalid' : ''; ?>" id="pass_baru2" name="pass_baru2" value="<?= set_value("pass_baru2", '', false) ?>" autofocus>
                <span toggle="#pass_baru2" class="text-primary fas fa-eye field-icon toggle-password"></span>
                <?php echo form_error('pass_baru2'); ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10 col-12">
                <button type="submit" name="simpan" class="btn btn-primary" value="simpan">Simpan</button>
                <button type="reset" class="btn btn-danger">Reset</button>
                <a href="<?= base_url('profil'); ?>" class="btn btn-dark">Kembali</a>
            </div>
        </div>
        <?= form_close(); ?>
        <!-- </form> -->
    </div>
</div>