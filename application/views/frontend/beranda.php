<div class="row-cols-sm-1 overflow-hidden my-3">
    <div class="">
        <section class="baner slider">
            <?php if (empty($banner)) : ?>
                <div class="col">
                    <img src="<?= base_url('uploads/baner/1.jpg'); ?>" class="rounded-lg">
                </div>
                <div class="col">
                    <img src="<?= base_url('uploads/baner/2.jpg'); ?>" class="rounded-lg">
                </div>
                <div class="col">
                    <img src="<?= base_url('uploads/baner/3.jpg'); ?>" class="rounded-lg">
                </div>
                <div class="col">
                    <img src="<?= base_url('uploads/baner/4.jpg'); ?>" class="rounded-lg">
                </div>
                <div class="col">
                    <img src="<?= base_url('uploads/baner/5.jpg'); ?>" class="rounded-lg">
                </div>
                <?php
            else :
                foreach ($banner as $ban) :
                ?>
                    <div class="col">
                        <img src="<?= "https://be.sipardi.id/" . $ban->gambar; ?>" class="rounded-lg">
                    </div>

            <?php endforeach;
            endif; ?>
        </section>
    </div>
</div>

<div class="row-cols-1" id="produk-beranda">

    <div class="col mb-3">
        <div class="row row-cols-md-4 row-cols-2">
            <div class="col mb-2">
                <a href="#" class="text-decoration-none text-dark" id="widgetBeranda">
                    <div class="rounded-lg d-block p-3 text-center text-capitalize">
                        <!-- <span class="fas fa-store fa-fw fa-lg fa-2x d-inline-block mb-2"></span> -->
                        <img src="<?= base_url('assets/img/pasar.png'); ?>" class="mb-1" alt="">
                        <small class="p-0 d-block">
                            Pasar
                        </small>
                    </div>
                </a>
            </div>

            <div class="col mb-2">
                <a href="#" class="text-decoration-none text-dark" id="widgetBeranda">
                    <div class="rounded-lg d-block p-3 text-center text-capitalize">
                        <img src="<?= base_url('assets/img/bantu-beli.png'); ?>" class="mb-1" alt="">
                        <!-- <span class="fas fa-people-carry fa-fw fa-lg fa-2x d-inline-block mb-2"></span> -->
                        <small class="p-0 d-block">
                            Bantu Beli
                        </small>
                    </div>
                </a>
            </div>

            <div class="col mb-2">
                <a href="#" class="text-decoration-none text-dark" id="widgetBeranda">
                    <div class="rounded-lg d-block p-3 text-center text-capitalize">
                        <img src="<?= base_url('assets/img/pulsa.png'); ?>" class="mb-1" alt="">
                        <!-- <span class="fas fa-mobile-alt fa-fw fa-lg fa-2x d-inline-block mb-2"></span> -->
                        <small class="p-0 d-block">
                            Pulsa &amp; Paket Data
                        </small>
                    </div>
                </a>
            </div>

            <div class="col mb-2">
                <a href="#" class="text-decoration-none text-dark" id="widgetBeranda">
                    <div class="rounded-lg d-block p-3 text-center text-capitalize">
                        <img src="<?= base_url('assets/img/pdam.png'); ?>" class="mb-1" alt="">
                        <!-- <span class="fas fa-faucet fa-fw fa-lg fa-2x d-inline-block mb-2"></span> -->
                        <small class="p-0 d-block">
                            PDAM
                        </small>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!-- Toko Populer -->
    <div class="col">
        <div class="row row-cols-1">
            <div class="col mb-0 px-2">
                <h5 class="d-block text-primary font-weight-bold">
                    Toko Populer
                    <small class="float-right">
                        <a class="btn btn-link text-danger text-decoration-none" href="<?= base_url("penjual"); ?>">Lihat Semua</a>
                    </small>
                </h5>
            </div>
            <div class="col mb-3">
                <div class="container-fluid">
                    <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-6 container-beranda">
                        <?php foreach ($toko_populer as $p) : ?>
                            <div class="col mb-3 p-1" id="product" data-toggle="tooltip" data-placement="top" title="<?= $p->nama_penjual; ?>">
                                <a href="<?= base_url($p->penjual_seo); ?>" class="text-decoration-none">
                                    <div class="card h-100 product rounded">
                                        <div class="thumbnail">
                                            <img src="<?= base_url('uploads/users/' . $p->foto); ?>" class="card-img-top rounded-top" alt="avatar">
                                        </div>
                                        <div class="card-body">
                                            <div class="card-text">
                                                <b class="text-truncate text-dark d-block"><?= $p->nama_penjual; ?></b>
                                                <small class="text-muted d-block text-truncate">
                                                    <?= ambil_nama_by_id("rb_desa", "nama_desa", "desa_id", $p->kelurahan); ?>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Toko Lain -->
    <div class="col">
        <div class="row row-cols-1">
            <div class="col mb-0 px-2">
                <h5 class="d-block text-primary font-weight-bold">
                    Toko Lain.
                    <small class="float-right">
                        <a class="btn btn-link text-danger text-decoration-none" href="<?= base_url("penjual"); ?>">Lihat Semua</a>
                    </small>
                </h5>
            </div>
            <div class="col mb-3">
                <div class="container-fluid">
                    <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-6 container-beranda">
                        <?php foreach ($toko_lainnya as $p) : ?>
                            <div class="col mb-3 p-1" id="product" data-toggle="tooltip" data-placement="top" title="<?= $p->nama_penjual; ?>">
                                <a href="<?= base_url($p->penjual_seo); ?>" class="text-decoration-none">
                                    <div class="card h-100 product rounded">
                                        <div class="thumbnail">
                                            <img src="<?= base_url('uploads/users/' . $p->foto); ?>" class="card-img-top rounded-top" alt="avatar">
                                        </div>
                                        <div class="card-body">
                                            <div class="card-text">
                                                <b class="text-truncate text-dark d-block"><?= $p->nama_penjual; ?></b>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Marketing Icons Section -->
    <?php if ($produk_populer) : ?>
        <div class="col">
            <div class="row row-cols-1">
                <div class="col mb-0 px-2">
                    <h5 class="d-block text-primary font-weight-bold">
                        Barang Populer
                        <small class="float-right">
                            <a class="btn btn-link text-danger text-decoration-none" href="<?= base_url("produk"); ?>">Lihat Semua</a>
                        </small>
                    </h5>
                </div>
                <div class="col mb-3">
                    <div class="container-fluid">
                        <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-6 container-beranda">
                            <?php foreach ($produk_populer as $p) :
                            ?>
                                <div class="col mb-3 p-1" id="product" data-toggle="tooltip" data-placement="top" title="<?= $p->nama_produk; ?>">
                                    <!-- <a href="<?= base_url('produk/' . $p->id_produk . '/' . $p->produk_seo); ?>" class="text-decoration-none"> -->
                                    <a href="<?= base_url($p->penjual_seo . '/' . $p->produk_seo); ?>" class="text-decoration-none">
                                        <div class="card h-100 product rounded">
                                            <?php
                                            $cek_operasional = cek_operasional($p->id_penjual);
                                            if ($cek_operasional) {
                                                echo "<div class='ribbon ribbon-top-left'><span><i class='fas fa-store-slash'></i> tutup</span></div>";
                                            }
                                            ?>

                                            <?php if ($p->diskon > 0) : ?>
                                                <div class='ribbon-discon ribbon-discon-top-left'><span> <?= $p->diskon; ?>% OFF</span></div>
                                            <?php endif; ?>

                                            <div class="thumbnail">
                                                <img src="<?= base_url('uploads/produk/' . $p->gambar); ?>" class="card-img-top rounded-top" height="">
                                            </div>
                                            <div class="card-body">
                                                <div class="card-text">
                                                    <b class="text-truncate text-dark d-block"><?= $p->nama_produk; ?></b>
                                                    <small class="text-truncate d-block"><?= $p->nama_penjual; ?></small>
                                                    <?php
                                                    if ($p->diskon > 0) :
                                                        $new_harga = $p->harga_konsumen - (($p->diskon / 100) * $p->harga_konsumen);
                                                    ?>
                                                        <s class="text-muted small"><?= rupiah($p->harga_konsumen); ?></s>
                                                        <h6 class="text-primary font-weight-bolder"><?= rupiah($new_harga); ?></h6>
                                                    <?php else : ?>
                                                        <h6 class="text-primary font-weight-bolder"><?= rupiah($p->harga_konsumen); ?></h6>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- <div class="col mb-3 text-center">
                <a href="<?= base_url("produk"); ?>" class="btn btn-lg btn-outline-primary">Lihat Semua Produk</a>
            </div> -->
            </div>
        </div>
    <?php endif; ?>

    <!-- barang terbaru -->
    <?php if ($produk_baru) : ?>
        <div class="col">
            <div class="row row-cols-1">
                <div class="col mb-0 px-2">
                    <h5 class="d-block text-primary font-weight-bold">
                        Barang terbaru
                        <small class="float-right">
                            <a class="btn btn-link text-danger text-decoration-none" href="<?= base_url("produk"); ?>">Lihat Semua</a>
                        </small>
                    </h5>
                </div>
                <div class="col mb-3">
                    <div class="container-fluid">
                        <div class="row row-cols-2 row-cols-sm-2 row-cols-md-4 row-cols-lg-6 container-beranda">
                            <?php foreach ($produk_baru as $p) : ?>
                                <div class="col mb-3 p-1" id="product" data-toggle="tooltip" data-placement="top" title="<?= $p->nama_produk; ?>">
                                    <!-- <a href="<?= base_url('produk/' . $p->id_produk . '/' . $p->produk_seo); ?>" class="text-decoration-none"> -->
                                    <a href="<?= base_url($p->penjual_seo . '/' . $p->produk_seo); ?>" class="text-decoration-none">
                                        <div class="card h-100 product rounded">
                                            <?php
                                            $cek_operasional = cek_operasional($p->id_penjual);
                                            if ($cek_operasional) {
                                                echo "<div class='ribbon ribbon-top-left'><span><i class='fas fa-store-slash'></i> tutup</span></div>";
                                            }
                                            ?>

                                            <?php if ($p->diskon > 0) : ?>
                                                <div class='ribbon-discon ribbon-discon-top-left'><span> <?= $p->diskon; ?>% OFF</span></div>
                                            <?php endif; ?>
                                            <div class="thumbnail">
                                                <img src="<?= base_url('uploads/produk/' . $p->gambar); ?>" class="card-img-top rounded-top">
                                            </div>
                                            <div class="card-body">
                                                <div class="card-text">
                                                    <b class="text-truncate text-dark d-block"><?= $p->nama_produk; ?></b>
                                                    <small class="text-truncate d-block"><?= $p->nama_penjual; ?></small>
                                                    <?php
                                                    if ($p->diskon > 0) :
                                                        $new_harga = $p->harga_konsumen - (($p->diskon / 100) * $p->harga_konsumen);
                                                    ?>
                                                        <s class="text-muted small"><?= rupiah($p->harga_konsumen); ?></s>
                                                        <h6 class="text-primary font-weight-bolder"><?= rupiah($new_harga); ?></h6>
                                                    <?php else : ?>
                                                        <h6 class="text-primary font-weight-bolder"><?= rupiah($p->harga_konsumen); ?></h6>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col mb-3 text-center">
                    <a href="<?= base_url("produk"); ?>" class="btn btn-lg btn-outline-primary">Lihat Semua Produk</a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>