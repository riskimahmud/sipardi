<?php $this->load->view('frontend/inc/head_html'); ?>
<div id="page-content">
	<div class="text-center mt-3">
		<!-- <h1 class="text-primary">SIPARDI</h1> -->
		<img src="<?= base_url("assets/img/top-logo.png"); ?>" alt="Logo SIPARDI" class="" width="200">
	</div>
	<div class="container mb-2 mt-4">
		<div class="row justify-content-md-center">
			<div class="col-md-5">
				<div class="card border shadow">
					<div class="card-body my-1">
						<?php
						$notif = $this->session->flashdata("notifikasi");
						if (!empty($notif)) {
							echo get_notif($notif['status'], $notif['pesan']);
						}
						?>
						<h4 class="text-center">Lupa Katasandi</h4>
						<span class="text-center text-muted d-block">Masuk ke sipardi? <a href="<?= base_url('login'); ?>">Masuk.</a></span>
						<form class="mt-3 px-3" method="POST" autocomplete="off">
							<div class="form-group">
								<label for="email">Email address</label>
								<input type="text" class="form-control <?= (form_error('email')) ? 'is-invalid' : ''; ?>" id="email" placeholder="name@example.com" name="email" value="<?php echo set_value('email', ''); ?>" autofocus>
								<?php echo form_error('email'); ?>
							</div>
							<div class="form-group">
								<button type="submit" name="lupa_password" value="Lupa Password" class="btn btn-outline-primary btn-block" id="logText">Kirim Password</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('frontend/inc/footer'); ?>
<?php $this->load->view('frontend/inc/foot_html'); ?>