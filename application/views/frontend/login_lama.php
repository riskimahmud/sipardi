<section class="login_area section--padding2">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<div id="responseDiv"><div id="message"></div></div>
				<form method="POST" id="logForm" autocomplete="off">
					<div class="cardify login">
						<div class="login--header">
							<h3>Welcome Back</h3>
							<p>You can sign in with your username</p>
						</div>
						<!-- end .login_header -->

						<div class="login--form">
							<div class="form-group">
								<label for="user_name">Username</label>
								<input id="user_name" type="text" class="text_field" name="username" placeholder="Enter your username...">
							</div>

							<div class="form-group">
								<label for="pass">Password</label>
								<input id="pass" type="password" class="text_field" name="password" placeholder="Enter your password...">
							</div>

							<div class="form-group">
								<div class="custom_checkbox">
									<input type="checkbox" id="ch2">
									<label for="ch2">
										<span class="shadow_checkbox"></span>
										<span class="label_text">Remember me</span>
									</label>
								</div>
							</div>

							<button class="btn btn--md btn--round" type="submit" id="logText">Login Now</button>

							<div class="login_assist">
								<p class="signup">Don't have an
									<a href="<?= site_url("frontend/register"); ?>">account</a>?</p>
							</div>
						</div>
						<!-- end .login--form -->
					</div>
					<!-- end .cardify -->
				</form>
			</div>
			<!-- end .col-md-6 -->
		</div>
		<!-- end .row -->
	</div>
	<!-- end .container -->
</section>