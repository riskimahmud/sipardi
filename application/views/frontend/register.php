<?php $this->load->view('frontend/inc/head_html'); ?>
<div id="page-content">
	<div class="text-center mt-3 d-block d-md-none">
		<!-- <h1 class="text-primary">SIPARDI</h1> -->
		<img src="<?= base_url("assets/img/top-logo.png"); ?>" alt="Logo SIPARDI" class="" width="100">
	</div>
	<div class="container mb-2">
		<div class="row row-cols-1 row-cols-md-2 align-bottom mt-5">
			<div class="col d-none d-md-block text-center">
				<img src="<?= base_url('assets/img/login-img.png'); ?>" class="img-fluid" alt="Sipardi">
			</div>
			<div class="col">
				<div class="card border shadow">
					<div class="card-body my-1">
						<?php
						$notif = $this->session->flashdata("notifikasi");
						if (!empty($notif)) {
							echo get_notif($notif['status'], $notif['pesan']);
						}
						?>
						<h4 class="text-center">Daftar Sekarang</h4>
						<span class="text-center text-muted d-block">Sudah punya akun Sipardi? <a href="<?= base_url('login'); ?>">Masuk.</a></span>
						<form class="mt-3 px-3" action="" method="POST" autocomplete="off">
							<div class="form-group">
								<label for="email">Email address</label>
								<input type="text" class="form-control <?= (form_error('email')) ? 'is-invalid' : ''; ?>" id="email" placeholder="name@example.com" name="email" value="<?php echo set_value('email', ''); ?>" autofocus>
								<?php echo form_error('email'); ?>
							</div>
							<div class="form-group">
								<label for="nama">Nama Lengkap</label>
								<input type="text" class="form-control <?= (form_error('nama')) ? 'is-invalid' : ''; ?>" id="nama" name="nama" value="<?php echo set_value('nama', ''); ?>">
								<?php echo form_error('nama'); ?>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control <?= (form_error('password')) ? 'is-invalid' : ''; ?>" id="password" name="password">
								<?php echo form_error('password'); ?>
							</div>
							<div class="form-group">
								<input type="submit" name="daftar" value="Daftar" class="btn btn-outline-primary btn-block">
							</div>
						</form>
						<small class="d-block text-muted text-center ">Dengan mendaftar, saya menyetujui</small>
						<small class="d-block text-muted text-center"><a href="#" id="term-condition">Syarat dan ketentuan</a> serta <a href="#" id="kebijakan-privasi">Kebijakan privasi</a></small>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" id="myModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-dialog-scrollable modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"></h5>
				<button type="button" class="close tutupModal" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary tutupModal" data-dismiss="modal">Mengerti</button>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('frontend/inc/footer'); ?>
<?php $this->load->view('frontend/inc/foot_html'); ?>