<div class="row p-2">
    <div class="col-md-12">
        <h5>
            <?= $method; ?> Produk
            <a href="<?= base_url('toko/produk'); ?>" class="btn btn-dark float-right btn-sm">Kembali</a>
        </h5>
        <hr>
        <?php echo validation_errors(); ?>
        <?= form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]); ?>
        <input type="hidden" name="method" value="<?= strtolower($method); ?>">
        <div class="form-group row">
            <label for="nama_produk" class="col-sm-2 col-form-label">Nama Produk</label>
            <div class="col-sm-10">
                <input type="text" class="form-control <?= (form_error('nama_produk')) ? 'is-invalid' : ''; ?>" id="nama_produk" name="nama_produk" placeholder="Masukkan Nama Produk" value="<?= set_value("nama_produk", ($method == "Ubah") ? $data->nama_produk : ''); ?>" autofocus>
                <?php echo form_error('nama_produk'); ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="kategori" class="col-sm-2 col-form-label">Kategori</label>
            <div class="col-sm-10">
                <select class="kategoriProduk form-control <?= (form_error('kategori')) ? 'is-invalid' : ''; ?>" id="kategori" name="kategori">
                    <?php if ($method == "Ubah") { ?>
                        <option selected value="<?= $data->id_kategori_produk; ?>"><?= get_kategori($data->id_kategori_produk); ?></option>
                    <?php } ?>
                </select>
                <?php echo form_error('kategori'); ?>
            </div>
        </div>
        <div class="form-group row">
            <label for="satuan" class="col-sm-2 col-form-label">Satuan</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-4">
                        <input type="text" class="form-control <?= (form_error('satuan')) ? 'is-invalid' : ''; ?>" id="satuan" name="satuan" placeholder="Cth: Pcs, Box, Paket" value="<?= set_value("satuan", ($method == "Ubah") ? $data->satuan : ''); ?>">
                        <?php echo form_error('satuan'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="harga" class="col-sm-2 col-form-label">Harga</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-4">
                        <input type="text" class="form-control <?= (form_error('harga')) ? 'is-invalid' : ''; ?>" id="harga" name="harga" onkeypress="hanyaAngka()" placeholder="Masukkan hanya angka." value="<?= set_value("harga", ($method == "Ubah") ? $data->harga_konsumen : ''); ?>">
                        <?php echo form_error('harga'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="harga" class="col-sm-2 col-form-label">Harga Tawar</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-8">
                        <input type="text" data-toggle="tooltip" data-placement="top" title="Masukkan Harga Minimal, Bukan Potongan" class="col-md-4 form-control <?= (form_error('harga_nego')) ? 'is-invalid' : ''; ?>" id="harga" name="harga_nego" onkeypress="hanyaAngka()" placeholder="Masukkan hanya angka." value="<?= set_value("harga_nego", ($method == "Ubah") ? $data->harga_nego : ''); ?>" aria-describedby="negoHelpBlock">
                        <small id="negoHelpBlock" class="form-text text-muted">
                            Kosongkan atau masukkan angka 0 jika tidak ingin menambah <b>tawar</b>.
                        </small>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="diskon" class="col-sm-2 col-form-label">Diskon</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-4">
                        <input type="text" class="form-control <?= (form_error('diskon')) ? 'is-invalid' : ''; ?>" id="diskon" name="diskon" onkeypress="hanyaAngka()" placeholder="Masukkan hanya angka" value="<?= set_value("diskon", ($method == "Ubah") ? $data->diskon : ''); ?>">
                        <?php echo form_error('diskon'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="berat" class="col-sm-2 col-form-label">Berat</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-4">
                        <input type="text" class="form-control <?= (form_error('berat')) ? 'is-invalid' : ''; ?>" id="berat" name="berat" onkeypress="hanyaAngka()" placeholder="Dalam satuan gram." value="<?= set_value("berat", ($method == "Ubah") ? $data->berat : ''); ?>">
                        <?php echo form_error('berat'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="kondisi" class="col-sm-2 col-form-label">Kondisi</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-4">
                        <select name="kondisi" id="kondisi" class="form-control">
                            <option value="">Pilih Kondisi</option>
                            <option value="Baru" <?= ((!empty($data)) && $data->kondisi == "Baru") ? 'selected' : ''; ?> <?= set_select("kondisi", ($method == "Ubah" && (!empty($data)) && $data->kondisi == "Baru") ? $data->kondisi : 'Baru'); ?>>Baru</option>
                            <option value="Bekas" <?= ((!empty($data)) && $data->kondisi == "Bekas") ? 'selected' : ''; ?> <?= set_select("kondisi", ($method == "Ubah" && (!empty($data)) && $data->kondisi == "Bekas") ? $data->kondisi : 'Bekas'); ?>">Bekas</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($method == "Ubah") {
            echo '<input type="hidden" name="stok" value="1">';
            echo '<input type="hidden" name="id_produk" value="' . $data->id_produk . '">';
        } else { ?>
            <div class=" form-group row">
                <label for="stok" class="col-sm-2 col-form-label">Stok</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control <?= (form_error('stok')) ? 'is-invalid' : ''; ?>" id="stok" name="stok" onkeypress="hanyaAngka()" placeholder="Masukkan stok awal." value="<?= set_value("stok", ''); ?>">
                            <?php echo form_error('stok'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="form-group row">
            <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
            <div class="col-sm-10">
                <textarea name="deskripsi" class="form-control" id="deskripsi">
                    <?= set_value('deskripsi', ($method == "Ubah") ? $data->deskripsi : ''); ?>
                </textarea>
            </div>
        </div>
        <div class="form-group row">
            <label for="gambar" class="col-sm-2 col-form-label">Gambar</label>
            <?php if ($method == "Tambah") { ?>
                <div class="col-sm-2">
                    <img src="<?= base_url("uploads/users/default.jpg"); ?>" class="img-thumbnail img-preview">
                </div>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input form-control <?= (form_error('gambar')) ? 'is-invalid' : ''; ?>" id="foto" name="gambar" onchange="previewImg()" aria-describedby="fotoHelpBlock">
                        <label class="custom-file-label" for="gambar">
                            Pilih Gambar
                        </label>
                        <small id="fotoHelpBlock" class="form-text text-muted">
                            <small id="gambarHelpBlock" class="form-text text-muted">
                                Format gambar JPG|PNG. Maksimal 5mb.
                            </small>
                            <span class="badge badge-danger">Direkomendasikan gambar yang berorientasi Landscape (200px x 180px)</span>
                        </small>
                        <?php echo form_error('gambar'); ?>
                    </div>
                </div>
            <?php } else {
            ?>
                <div class="col-sm-2">
                    <img src="<?= base_url("uploads/produk/" . $data->gambar); ?>" class="img-thumbnail img-preview">
                </div>
                <div class="col-sm-8">
                    <div class="custom-file">
                        <input type="hidden" name="gambar_lama" value="<?= $data->gambar; ?>">
                        <input type="file" class="custom-file-input form-control <?= (form_error('gambar')) ? 'is-invalid' : ''; ?>" id="foto" name="gambar" onchange="previewImg()" aria-describedby="gambarHelpBlock">
                        <label class="custom-file-label" for="gambar">
                            <?= ($data->gambar == 'default.jpg' || $data->gambar == "") ? 'Pilih Gambar' : $data->gambar; ?>
                        </label>
                        <small id="gambarHelpBlock" class="form-text text-muted">
                            Kosongkan jika tidak ingin mengganti gambar. Format gambar JPG|PNG. Maksimal 5mb.
                            <span class="badge badge-danger d-block">Direkomendasikan gambar yang berorientasi Landscape (200px x 180px)</span>
                        </small>
                        <?php echo form_error('gambar'); ?>
                    </div>
                </div>
            <?php
            } ?>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary"><?= $method; ?></button>
                <button type="reset" class="btn btn-secondary">Batal</button>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
</div>