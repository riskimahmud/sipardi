<?php
// header("Cache-Control: no-cache, must-revalidate");
// header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
// header("Content-Type: application/xml; charset=utf-8");
$this->load->view('frontend/inc/head_html');
?>
<?php $this->load->view('frontend/inc/navbar'); ?>
<main class="" id="page-content">
	<?php if (cekPopUp()) { ?>
		<div class="container-popup">
			<div class="popup">
				<span id="closePopUp">
					x
				</span>
				<img src="<?= 'https://be.sipardi.id/' . ambil_nama_by_id('tbl_iklan', 'gambar', 'is_default', '1'); ?>" alt="iklan" class="img-fluid">
			</div>
		</div>
	<?php } ?>
	<a href="https://wa.me/+6282187468505" class="ignielToTop">
		<img src="<?= base_url('assets/img/icon.png'); ?>" alt="sipardi">
		<span id="textCallMe">Hubungi Kami</span>
	</a>
	<?php $this->load->view('frontend/inc/header'); ?>
	<?php if ($this->session->flashdata("notifikasi")) { ?>
		<div class="container mt-2">
			<div class="alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?>">
				<?= $this->session->flashdata("notifikasi")["msg"]; ?>
			</div>
		</div>
	<?php } ?>
	<?php $this->load->view('frontend/' . $page); ?>
	<div class="modal fade" tabindex="-1" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
</main>
<?php $this->load->view('frontend/inc/footer'); ?>
<?php $this->load->view('frontend/inc/foot_html'); ?>