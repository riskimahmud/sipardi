<?php
class Frontend extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		// if ($this->session->userdata('user')) {
		// 	$this->load->helper('url');
		// 	$this->session->set_userdata('last_page', current_url());
		// 	redirect('/login');
		// }
	}

	public function index()
	{
		$this->session->unset_userdata("cari");
		$this->session->unset_userdata("cari_filter");
		$this->session->unset_userdata("cari_penjual");
		if (!$this->session->userdata('last_page')) {
			$data['title']			=	"Beranda";
			$data['page']			=	"/beranda";
			$data['banner']			=	$this->crud_model->select_all("tbl_banner");
			$data['populer']		=	$this->crud_model->select_custom("select id_produk, nama_produk, harga_konsumen, produk_seo, gambar, nama_penjual, penjual.id_penjual, penjual_seo from produk join penjual on produk.id_penjual = penjual.id_penjual order by hits DESC limit 4");
			$data['toko_populer']	=	$this->crud_model->select_custom("select nama_penjual, penjual_seo, penjual.foto, sum(hits) as hits, sum(terjual) as terjual, kelurahan from produk right join penjual on produk.id_penjual = penjual.id_penjual where penjual.kelurahan <> '0' group by penjual.id_penjual order by terjual DESC, hits DESC limit 12");
			$data['toko_lainnya']	=	$this->crud_model->select_custom("select nama_penjual, penjual_seo, penjual.foto from produk right join penjual on produk.id_penjual = penjual.id_penjual group by penjual.id_penjual order by RAND() DESC limit 12");
			$data['produk_populer']	=	$this->crud_model->select_custom("select id_produk, nama_produk, harga_konsumen, produk_seo, gambar, nama_penjual, penjual_seo, penjual.id_penjual, diskon from produk join penjual on produk.id_penjual = penjual.id_penjual where status_produk = '1' order by produk.terjual DESC limit 12");
			$data['produk_baru'] 	=	$this->crud_model->select_custom("select id_produk, nama_produk, harga_konsumen, produk_seo, gambar, nama_penjual, penjual_seo, penjual.id_penjual, diskon from produk join penjual on produk.id_penjual = penjual.id_penjual where status_produk = '1' order by waktu_input DESC limit 12");
			$this->load->view("frontend/main", $data);
		} else {
			$page	=	$this->session->userdata('last_page');
			// echo $page;
			$this->session->unset_userdata('last_page');
			redirect($page);
		}
	}

	// verifikasi email pendaftar
	public function verifikasi($kode = null)
	{
		$data 			=	$this->crud_model->select_one("verifikasi_email", "kode_referal", $kode);
		if ($kode === null || (empty($data))) {
			redirect("e404");
		} else {
			$this->crud_model->update("users", ["status_user" => "1"], "id_konsumen", $data->id_konsumen);
			$this->crud_model->hapus_id("verifikasi_email", "id_konsumen", $data->id_konsumen);
			$this->load->view("frontend/verifikasi");
		}
	}

	// ======================================================================================================================================
	// Produk
	// ======================================================================================================================================

	public function produk()
	{
		$this->session->unset_userdata("cari_penjual");
		$likes = null;
		$where = [];
		$likes = [];
		$join = "0";
		$where["status_produk"] = "1";
		$order = ["key" => "hits", "value" => "DESC"];
		$kategori = "";
		$data['page']		=	"/produk/index";
		if ($this->input->get("cari")) {
			if ($this->input->get("sortir")) {
				$order = ["key" => "harga_konsumen", "value" => $this->input->get("sortir")];
			}
			if ($this->input->get("kategori") != "") {
				$path = $this->crud_model->select_by_field_row("rb_kategori_produk", "path", ["id_kategori_produk" => $this->input->get("kategori")]);
				$kategori = $this->input->get("kategori");
				$likes["rb_kategori_produk.path"] = $path->path;
				$join = "1";
			}
			$likes["nama_produk"] = $this->input->get("keyword");
			$this->session->set_userdata("cari", [
				"where" => $where,
				"likes" => $likes,
				"order" => $order,
				"kategori" => $kategori,
				"keyword" => $this->input->get("keyword")
			]);
		}
		if ($this->input->get("hapus")) {
			$this->session->unset_userdata("cari");
			redirect("produk");
		}

		$this->load->library('pagination');

		$config['base_url'] = base_url() . "/produk";
		if ($this->session->userdata("cari")) {
			$data['title'] = "Cari Produk '" . cari("keyword") . "'";
			$config['total_rows'] = $this->crud_model->select_all_where_array_likes_num_row("produk", $this->session->userdata("cari")["where"], $this->session->userdata("cari")["likes"], $join);
			// $config['total_rows'] = 10;
		} else {
			$data['title']		=	"Semua Produk";
			$config['total_rows'] = $this->crud_model->select_all_where_array_likes_num_row("produk", $where, $likes);
		}
		$config['per_page'] = 30;
		$config['num_links'] = 5;
		$offset = ($this->uri->segment(2)  == 0) ? 0 : ($this->uri->segment(2) * $config['per_page']) - $config['per_page'];

		if ($this->session->userdata("cari")) {
			$produk		=	$this->crud_model->select_paging_where("produk", $this->session->userdata("cari")["where"], $config['per_page'], $offset, $this->session->userdata("cari")["order"], $this->session->userdata("cari")["likes"], $join);
		} else {
			$produk		=	$this->crud_model->select_paging_where("produk", $where, $config['per_page'], $offset, $order, $likes);
		}

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['produk'] = $produk;
		// print_r($where);
		// print_r($likes);
		// print_r($join);
		$this->load->view("frontend/main", $data);
	}

	// detail produk
	public function detail_produk($penjual_seo = null, $produk_seo = null)
	{
		$penjual			=	$this->crud_model->select_one("penjual", "penjual_seo", $penjual_seo);
		if ($penjual_seo === null || (empty($penjual))) {
			redirect("e404");
		} else {
			$produk 			=	$this->crud_model->select_one_where_array("produk", ["produk_seo" => $produk_seo, "id_penjual" => $penjual->id_penjual]);
			if ($produk_seo === null || (empty($produk))) {
				redirect("e404");
			} else {
				if ($produk->status_produk == "0") {
					redirect("e404");
					// redirect("produk");
				}
				$data_update 		=	["hits" => $produk->hits + 1];
				$this->crud_model->update("produk", $data_update, "id_produk", $produk->id_produk);
				$stok		=	$this->crud_model->select_sum("stok", "stok", ["id_produk" => $produk->id_produk]);
				$terjual	=	$this->crud_model->select_sum("rb_penjualan_detail", "qty", ["id_produk" => $produk->id_produk, "status >" => 0], ["table" => "rb_penjualan", "key" => "id_penjualan"]);
				$data['terjual']	=	$terjual;
				$data['stok']		=	$stok - $terjual;
				$data['penjual']	=	$this->crud_model->select_one("penjual", "id_penjual", $produk->id_penjual);
				$data['title']		=	$produk->nama_produk;
				$data['page']		=	"/produk/detail";
				$data['produk']		=	$produk;
				$data['diskusi']	=	$this->crud_model->select_all_where_array_limit("diskusi", ["id_produk" => $produk->id_produk, "reply_for" => null], "create_at", "DESC", "10");
				$this->load->view("frontend/main", $data);
			}
		}
	}

	public function kirim_diskusi()
	{
		$produk = $this->crud_model->select_one("produk", "id_produk", $this->input->post("id_produk"));
		$penjual = $this->crud_model->select_by_field_row("penjual", "penjual_seo", ["id_penjual" => $produk->id_penjual]);
		$data = [
			"id_produk" => $this->input->post("id_produk", true),
			"pesan" => $this->input->post("pesan", true),
			"pengirim" => user("id_konsumen")
		];
		$kirim = $this->crud_model->insert("diskusi", $data);
		if ($kirim) {
			$notifikasi		=	array(
				"status"	=>	"success", "msg"	=>	"Pesan berhasil dikirm"
			);
		} else {
			$notifikasi		=	array(
				"status"	=>	"danger", "msg"	=>	"Pesan gagal dikirim"
			);
		}
		$this->session->set_flashdata("notifikasi", $notifikasi);
		redirect($penjual->penjual_seo . "/" . $produk->produk_seo);
	}

	// ======================================================================================================================================
	// end Produk
	// ======================================================================================================================================



	// ======================================================================================================================================
	// Penjualan
	// ======================================================================================================================================

	public function penjual()
	{
		$this->session->unset_userdata("cari");
		$likes = null;
		$where = ["status_penjual <>" => "2"];
		$order = ["key" => "tanggal_daftar", "value" => "DESC"];
		if ($this->input->get("cari_penjual")) {
			$likes = ["nama_penjual" => $this->input->get("keyword", "")];
			$this->session->set_userdata("cari_penjual", ["where" => $where, "likes" => $likes, "keyword" => $this->input->get("keyword", "")]);
		}
		if ($this->input->get("hapus")) {
			$this->session->unset_userdata("cari_penjual");
			redirect("penjual");
		}
		$data['title']		=	"Semua Penjual";
		$data['page']		=	"/penjual/index";

		$this->load->library('pagination');

		$config['base_url'] = base_url() . "/penjual";
		if ($this->session->userdata("cari_penjual")) {
			$data['title'] = "Cari Penjual '" . cari_penjual("keyword") . "'";
			$config['total_rows'] = $this->crud_model->select_all_where_array_likes_num_row("penjual", $this->session->userdata("cari_penjual")["where"], $this->session->userdata("cari_penjual")["likes"]);
		} else {
			$config['total_rows'] = $this->crud_model->select_all_where_array_likes_num_row("penjual", $where, $likes);
		}
		$config['per_page'] = 30;
		$config['num_links'] = 5;
		$offset = ($this->uri->segment(2)  == 0) ? 0 : ($this->uri->segment(2) * $config['per_page']) - $config['per_page'];

		if ($this->session->userdata("cari_penjual")) {
			$penjual		=	$this->crud_model->select_paging_where("penjual", $this->session->userdata("cari_penjual")["where"], $config['per_page'], $offset, $order, $this->session->userdata("cari_penjual")["likes"]);
		} else {
			$penjual		=	$this->crud_model->select_paging_where("penjual", $where, $config['per_page'], $offset, $order, $likes);
		}

		$this->pagination->initialize($config);

		$data['pagination'] = $this->pagination->create_links();
		$data['penjual'] = $penjual;
		// print_r($likes);
		$this->load->view("frontend/main", $data);
	}

	// detail produk
	public function detail_penjual($id_penjual = null)
	{
		// $penjual 			=	$this->crud_model->select_one("penjual", "id_penjual", $id_penjual);
		$penjual 			=	$this->crud_model->select_one("penjual", "penjual_seo", $id_penjual);
		// echo $id_penjual;
		if ($id_penjual === null || (empty($penjual))) {
			redirect("e404");
		} else {
			$penjual 				=	$this->crud_model->select_one("penjual", "id_penjual", $penjual->id_penjual);
			$data['penjual']		=	$penjual;
			$data['pemilik']		=	$this->crud_model->select_by_field_row("users", "nama_lengkap, last_login", ["id_konsumen" => $penjual->id_user]);
			$data['produk']			=	$this->crud_model->select_by_field("produk", "id_produk, produk_seo, nama_produk, harga_konsumen, diskon, gambar", ["id_penjual" => $penjual->id_penjual, "status_produk" => "1"], "terjual", "DESC", "24", "0");
			$data['title']			=	$penjual->nama_penjual;
			$data['page']			=	"/penjual/detail";
			$data['page_data']		=	"1";
			$data['jumlah_produk']	=	$this->crud_model->select_all_where_array_num_row("produk", ["id_penjual" => $penjual->id_penjual, "status_produk" => "1"]);
			$data['penjual']		=	$penjual;
			$this->load->view("frontend/main", $data);
		}
	}
	// ======================================================================================================================================
	// end Penjualan
	// ======================================================================================================================================
}
