<?php
class Ajax extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
	}

	public function index($id_produk)
	{
		if ($this->session->userdata('user')) {
			redirect('/login');
		}
	}

	// cek jumlah barang yang ada di keranjang
	public function cek_keranjang()
	{
		$ret = [];
		$keranjang = $this->crud_model->select_all_where_num_row("keranjang", "id_pembeli", user("id_konsumen"));
		if ($keranjang > 0) {
			$ret = [
				"status" => 1,
				"msg" => "Keranjang ada isi",
				"data" => $keranjang
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Keranjang kosong",
				"data" => ''
			];
		}
		echo json_encode($ret);
	}

	// ambil kecamatan
	public function ambil_kecamatan()
	{
		$ret = [];
		$data = $this->crud_model->select_all_where("rb_kecamatan", "kota_id", $this->input->post("kota_id", true));
		if ($data) {
			$ret = [
				"status" => 1,
				"msg" => "Kecamatan ditemukan",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Kecamatan tidak ditemukan",
				"data" => []
			];
		}
		echo json_encode($ret);
	}

	// ambil kelurahan
	public function ambil_desa()
	{
		$ret = [];
		$data = $this->crud_model->select_all_where("rb_desa", "kecamatan_id", $this->input->post("kecamatan_id", true));
		if ($data) {
			$ret = [
				"status" => 1,
				"msg" => "Desa ditemukan",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Desa tidak ditemukan",
				"data" => []
			];
		}
		echo json_encode($ret);
	}

	// ambil ongkir
	public function ambil_ongkir()
	{
		$ret = [];
		$idPenjual = $this->input->post("idPenjual", true);
		$kelurahanPenjual = $this->input->post("kelurahanPenjual", true);
		$kelurahanPembeli = $this->input->post("kelurahanPembeli", true);
		$penjual = $this->crud_model->select_one_where_array("rb_reseller_cod", ["id_reseller" => $idPenjual, "desa_id" => $kelurahanPembeli]);
		if ($penjual) {
			$ret = [
				"status" => 1,
				"msg" => "Ongkir Ditemukan",
				"data" => $penjual
			];
		} else {
			$admin = $this->crud_model->select_one_where_array("cod_admin", ["kelurahan_penjual" => $kelurahanPenjual, "kelurahan_pembeli" => $kelurahanPembeli]);
			if ($admin) {
				$ret = [
					"status" => 1,
					"msg" => "Ongkir Ditemukan",
					"data" => $admin
				];
			} else {
				$ret = [
					"status" => 0,
					"msg" => "Ongkir tidak ditemukan",
					"data" => []
				];
			}
		}
		// }
		echo json_encode($ret);
	}

	// ambil sub kategori
	public function ambil_subkategori()
	{
		$ret = [];
		$id_parent = $this->input->post("id_parent", true);
		$data = $this->crud_model->select_all_where_array("rb_kategori_produk", ["id_parent" => $id_parent]);
		if ($data) {
			$ret = [
				"status" => 1,
				"msg" => "sub kategori ditemukan",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "sub kategori tidak ditemukan",
				"data" => []
			];
		}
		// }
		echo json_encode($ret);
	}

	public function ambil_kategori()
	{
		$ret = [];
		if ($this->input->post("key", true)) {
			$data = $this->crud_model->select_like("rb_kategori_produk", "nama_kategori", $this->input->post("key"));
		} else {
			$data = $this->crud_model->select_custom("select * from rb_kategori_produk where id_parent is null limit 5");
		}
		foreach ($data as $d) {
			$ret[] = [
				"id" => $d->id_kategori_produk,
				// "text" => $d->nama_kategori
				"text" => $d->nama_kategori
			];
			if ($d->id_parent != "") {
				$ret[]["description"] = "dalam kategori (" . get_kategori($d->id_parent) . ")";
			}
		}
		echo json_encode($ret);
	}

	// cek kelurahan zonasi cod agar tidak menginput kelurahan yang sama
	public function cek_kelurahan()
	{
		$ret = [];
		$kelurahan_id = $this->input->post("kelurahan_id", true);
		$data = $this->crud_model->select_one_where_array("rb_reseller_cod", ["desa_id" => $kelurahan_id, "id_reseller" => penjual("id_penjual")]);
		if ($data) {
			$ret = [
				"status" => 1,
				"msg" => "Kelurahan Sudah Di Input",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "kelurahan belum diinput",
			];
		}
		// }
		echo json_encode($ret);
	}

	// ambil pasar untuk mendaftar sebagai penjual
	public function ambil_pasar()
	{
		$ret = [];
		$data = $this->crud_model->select_all("pasar");
		if ($data) {
			$ret = [
				"status" => 1,
				"msg" => "Pasar ditemukan",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "pasar tidak ditemukan",
			];
		}
		echo json_encode($ret);
	}

	// kirim pesan diskusi
	public function kirim_diskusi()
	{
		$ret = [];
		$data = [
			"id_produk" => $this->input->post("id_produk"),
			"pesan" => $this->input->post("pesan", true),
			"pengirim" => user("id_konsumen")
		];

		$produk = $this->crud_model->select_one("produk", "id_produk", $data["id_produk"]);
		// $id_penjual = $this->crud_model->select_by_field_row("produk", "id_penjual", ["id_produk" => $data["id_produk"]]);
		$id_user = $this->crud_model->select_by_field_row("penjual", "id_user", ["id_penjual" => $produk->id_penjual]);
		$email_penjual = $this->crud_model->select_by_field_row("users", "email", ["id_konsumen" => $id_user->id_user]);

		$simpan = $this->crud_model->insert("diskusi", $data);
		if ($simpan) {
			$this->load->model("email_model");
			$pesan = 'Seseorang telah mengirim diskusi baru pada produk ' . $produk->nama_produk . '<br>';
			$pesan .= '<a href="' . base_url("toko/produk/" . $produk->id_produk . "/" . $produk->produk_seo) . '">Klik Disini</a> untuk melihat';
			$this->email_model->kirim_email($email_penjual->email, "Diskusi Barang Baru.", $pesan);

			$ret = [
				"status" => 1,
				"msg" => "Pesan berhasil dikirim",
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Pesan gagal dikirim",
			];
		}
		echo json_encode($ret);
	}

	// kirim pesan balasan diskusi
	public function kirim_balasan_diskusi()
	{
		$ret = [];
		$data = [
			"id_produk" => $this->input->post("id_produk"),
			"reply_for" => $this->input->post("id_diskusi", true),
			"pesan" => $this->input->post("pesan", true),
			"pengirim" => penjual("id_penjual")
		];

		$simpan = $this->crud_model->insert("diskusi", $data);
		if ($simpan) {
			$this->crud_model->update("diskusi", ["status" => "1"], "id_diskusi", $data["reply_for"]);
			$ret = [
				"status" => 1,
				"msg" => "Pesan berhasil dikirim",
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Pesan gagal dikirim",
				"data" => $data
			];
		}
		echo json_encode($ret);
	}

	// ambil kontent
	public function ambil_konten()
	{
		$ret = [];
		$page = $this->input->post("field", true);
		$data = $this->crud_model->select_by_field_row("tbl_konten", $page, ["id_konten" => "1"]);

		if (!empty($data)) {
			$ret = [
				"status" => 1,
				"msg" => "Konten ditemukan",
				"data" => $data->$page
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Konten tidak ditemukan",
				"data" => ""
			];
		}
		echo json_encode($ret);
	}

	// tawar produk
	public function tawar_produk()
	{
		$ret = [];
		$id_produk = $this->input->post("idproduk", true);
		$harga = $this->input->post("harga", true);

		$cek = $this->crud_model->cek_data_where_array("tawar", ["id_produk" => $id_produk, "id_pembeli" => user("id_konsumen")]);
		if ($cek) {
			$cek_harga = $this->crud_model->cek_data_where_array("produk", ["id_produk" => $id_produk, "harga_nego >" => $harga]);
			if ($cek_harga) {
				$simpan = $this->crud_model->insert("tawar", ["id_produk" => $id_produk, "id_pembeli" => user("id_konsumen"), "harga_tawar" => $harga]);
				if ($simpan) {
					$ret = [
						"status" => 1,
						"msg" => "Tawaran Diterima",
						"data" => ""
					];
				} else {
					$ret = [
						"status" => 0,
						"msg" => "Tawaran Anda Ditolak",
						"data" => ""
					];
				}
			} else {
				$ret = [
					"status" => 0,
					"msg" => "Tawaran Anda Ditolak",
					"data" => ""
				];
			}
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Anda Telah Melakukan Penawaran Untuk Produk Ini.",
				"data" => ""
			];
		}
		echo json_encode($ret);
	}

	// autocomplete
	public function autocomplete()
	{
		$query = $this->input->get('query');
		$this->db->select('nama_produk');
		$this->db->like('nama_produk', $query);
		$this->db->group_by("nama_produk");
		$this->db->order_by("hits", "DESC");
		$this->db->order_by("terjual", "DESC");


		$hasil = $this->db->get("produk", 10)->result();
		$data = array();
		foreach ($hasil as $hsl) {
			$data[] = $hsl->nama_produk;
		}

		echo json_encode($data);
	}

	// ambil produk lain berdasarkan id_produk
	public function ambil_produk_lain()
	{
		$ret = [];
		$idpenjual = $this->input->post("idpenjual", true);
		$page = $this->input->post("page", true);
		// $data = $this->crud_model->select_by_field_row("tbl_konten", $page, ["id_konten" => "1"]);
		$this->db->select('id_produk, produk_seo, nama_produk, harga_konsumen, diskon, gambar');
		$this->db->where("id_penjual", $idpenjual);
		$this->db->where("status_produk", "1");
		$this->db->order_by("terjual", "DESC");


		$data = $this->db->get("produk", 24, $page)->result();

		if (!empty($data)) {
			$ret = [
				"status" => 1,
				"msg" => "Produk ditemukan",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "produk tidak ditemukan",
				"data" => ""
			];
		}
		echo json_encode($ret);
	}

	// ambil produk lain berdasarkan id_produk
	public function cari_produk_by_penjual()
	{
		$ret = [];
		$idpenjual = $this->input->post("idpenjual", true);
		$keyword = $this->input->post("keyword", true);
		// $data = $this->crud_model->select_by_field_row("tbl_konten", $page, ["id_konten" => "1"]);
		$this->db->select('id_produk, produk_seo, nama_produk, harga_konsumen, diskon, gambar');
		$this->db->like("nama_produk", $keyword);
		$this->db->where("id_penjual", $idpenjual);
		$this->db->where("status_produk", "1");
		$this->db->order_by("terjual", "DESC");

		$data = $this->db->get("produk")->result();

		if (!empty($data)) {
			$ret = [
				"status" => 1,
				"msg" => "Produk ditemukan",
				"data" => $data
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "produk tidak ditemukan",
				"data" => ""
			];
		}
		echo json_encode($ret);
	}

	public function notifikasi()
	{
		$this->load->view('frontend/inc/notifikasi');
	}

	public function tutup_iklan()
	{
		$this->session->sess_expiration = '60';
		$this->session->set_userdata('iklan', 'Y');
	}

	// tolak pesanan
	public function tolak_pesanan()
	{
		$ret = [];
		$data = [
			"status" => "0",
			"keterangan_penjual" => $this->input->post("pesan", true),
		];

		$simpan = $this->crud_model->update("rb_penjualan", $data, "kode_transaksi", $this->input->post('kode_transaksi', true));
		if ($simpan) {
			$penjualan 			=	$this->crud_model->select_one("rb_penjualan", "kode_transaksi", $this->input->post('kode_transaksi', true));
			$penjual = $this->crud_model->select_one("penjual", "id_penjual", $penjualan->id_penjual);
			$pembeli = $this->crud_model->select_one("users", "id_konsumen", $penjualan->id_pembeli);
			$this->crud_model->insert("rb_penjualan_riwayat", [
				"id_penjualan_riwayat"	=>	$this->crud_model->cek_id("rb_penjualan_riwayat", "id_penjualan_riwayat"),
				"id_penjualan"	=>	$penjualan->id_penjualan,
				"status"	=>	"0",
				"keterangan" => "Pesanan Anda Telah Ditolak oleh Penjual. Keterangan: " . $data["keterangan_penjual"]
			]);

			// Kirim SMS
			$this->load->library('sms');
			$telp_pembeli = hp($pembeli->no_hp);
			$pesan = "Pesanan Anda Telah Ditolak oleh Penjual. Dengan kode transaksi : " . $penjualan->kode_transaksi . ". Detail: " . base_url("pembelian/detail/" . $penjualan->kode_transaksi);
			$this->sms->kirimSms($telp_pembeli, $pesan);
			// Kirim SMS

			$data_notifikasi = [
				"tujuan" => $penjualan->id_pembeli,
				"dari"	=>	$penjual->nama_penjual,
				"judul" => "Pembelian",
				"pesan" => "Pesanan Anda Telah Ditolak oleh Penjual",
				"link" => base_url("pembelian/detail/" . $penjualan->kode_transaksi),
				"status" => "0"
			];

			$this->crud_model->insert("notifikasi", $data_notifikasi);

			require APPPATH . 'views/vendor/autoload.php';

			$options = array(
				'cluster' => 'ap1',
				'useTLS' => true
			);
			$pusher = new Pusher\Pusher(
				'3c6926b288c9e03c843e',
				'5e63d5100e294b35d52d',
				'1138134',
				$options
			);

			$data['message'] = 'notifikasi';
			$pusher->trigger('my-channel', 'my-event', $data);

			$ret = [
				"status" => 1,
				"msg" => "Pesan berhasil dikirim",
			];
		} else {
			$ret = [
				"status" => 0,
				"msg" => "Pesan gagal dikirim",
				"data" => $data
			];
		}
		echo json_encode($ret);
	}

	// cek promo
	public function cek_promo()
	{
		$ret = [];
		$jumBelanja	=	$this->crud_model->select_sum('keranjang', 'total', ["id_pembeli" => user("id_konsumen")]);
		$cek_data	=	$this->crud_model->select_one_where_array('promo', [
			// "kuota >" => 0,
			"kode_referal"	=>	$this->input->post('kode', true)
		]);
		if (empty($cek_data)) {
			$ret = [
				"status" => 0,
				"keterangan" => "Kode tidak ditemukan",
			];
		} else {
			if ($cek_data->minimal_belanja > $jumBelanja) {
				$ret = [
					"status" => 1,
					"badge"	=>	"warning",
					"keterangan" => "Tidak mencapai minimal belanja. (Minimal belanja " . rupiah($cek_data->minimal_belanja) . ")",
				];
			} elseif ($cek_data->kuota == 0) {
				$ret = [
					"status" => 1,
					"badge"	=>	"warning",
					"keterangan" => "Promo sudah habis.",
				];
			} else {
				$ret = [
					"status" => 1,
					"badge"	=>	"success",
					"keterangan" => $cek_data->keterangan,
				];
			}
		}
		echo json_encode($ret);
	}
}
