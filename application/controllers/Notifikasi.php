<?php
class Notifikasi extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		if (!$this->session->userdata('user')) {
			$this->load->helper('url');
			$this->session->set_userdata('last_page', current_url());
			redirect('/login');
		}
	}

	public function index()
	{
		$data['title']		=	"Notifikasi";
		$data['page']		=	"notifikasi";
		// $data['subpage']	=	"profil";
		$data['data']		=	$this->crud_model->select_all_where_array_order("notifikasi", ["tujuan" => user("id_konsumen"), "status" => "0"], "create_at", "DESC");

		$this->load->view("frontend/main", $data);
	}

	public function baca($id = null)
	{
		if ($id === null) {
			redirect('frontend');
		} else {
			$where = [
				"tujuan" => user("id_konsumen"),
				"notifikasi_id" => $id
			];
			$cek_data = $this->crud_model->cek_data_where_array("notifikasi", $where);
			if ($cek_data) {
				redirect('frontend');
			} else {
				$this->crud_model->update_where_array("notifikasi", ["status" => "1"], $where);
				$data = $this->crud_model->select_one_where_array("notifikasi", $where);
				redirect($data->link);
			}
		}
	}
}
