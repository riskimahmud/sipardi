<?php
class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
	}

	public $base	= 'frontend';
	public $konten	= '/frontend';

	public function index()
	{
		if ($this->session->userdata('user')) {
			redirect('frontend');
		} else {
			$data['title']		=	"Login";
			$this->load->view("frontend/login", $data);
		}
	}

	public function do_login()
	{
		$output = array('error' => false);

		$email = $this->input->post('email', true);
		$password = hash("sha512", md5($this->input->post('password', true)));
		$cek_email = $this->crud_model->cek_data_existing("users", "email", $email);
		if ($cek_email) {
			$data = $this->users_model->login($email, $password);
			if ($data) {
				if ($data['status_user'] == "0") {
					$output['error'] = true;
					$output['message'] = 'Email anda belum diverifikasi';
				} else {
					$update	= array(
						"last_login"	=>	date("Y-m-d H:i:s")
					);
					$this->crud_model->update("users", $update, "id_konsumen", $data['id_konsumen']);
					$this->session->set_userdata('user', $data);
					if ($data['penjual'] == "1") {
						$penjual = $this->crud_model->select_one("penjual", "id_user", $data['id_konsumen']);
						$this->session->set_userdata("penjual", [
							"id_penjual" => $penjual->id_penjual,
							"nama_penjual" => $penjual->nama_penjual,
							"status_penjual" => $penjual->status_penjual
						]);
					}
					$output['message'] = 'Logging in. Please wait...';
				}
			} else {
				$output['error'] = true;
				$output['message'] = 'Password Salah';
			}
		} else {
			$output['error'] = true;
			$output['message'] = 'Email belum terdaftar';
		}

		echo json_encode($output);
	}

	public function logout()
	{
		$notifikasi		=	array(
			"status"	=>	"info", "msg"	=>	"Selamat Tinggal. Anda Berhasil Keluar"
		);

		$this->session->set_flashdata("notifikasi", $notifikasi);
		// $this->session->unset_userdata('user');
		$this->session->sess_destroy();
		redirect();
	}
}
