<?php
class Forget extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
	}

	public function index()
	{
		if ($this->session->userdata('user')) {
			redirect('frontend');
		} else {
			if ($this->input->post('lupa_password')) {
				$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|required|callback_email_check');
				$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');

				if ($this->form_validation->run() == FALSE) {
					$data['title']		=	"Lupa Katasandi";
					$this->load->view("frontend/lupa_katasandi", $data);
				} else {
					$pass_baru = strtolower(generateRandomString(8));
					$data	=	array(
						"password"		=>	hash("sha512", md5($pass_baru))
					);
					$daftar		=	$this->crud_model->update("users", $data, "email", $this->input->post("email", true));
					if ($daftar) {
						$this->load->model("email_model");
						$tujuan = $this->input->post("email", true);
						$judul = "Password Baru";
						$content = 'Password baru anda adalah : ' . $pass_baru;
						$this->email_model->kirim_email($tujuan, $judul, $content);
						$notifikasi		=	array(
							"status"	=>	1, "pesan"	=>	"Password anda telah dikirimkan ke email anda."
						);
					} else {
						$notifikasi		=	array(
							"status"	=>	0, "pesan"	=>	"Password baru gagal dikirim"
						);
					}
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect("forget");
				}
			} else {
				$data['title']		=	"Lupa Katasandi";
				$this->load->view("frontend/lupa_katasandi", $data);
			}
		}
	}

	public function email_check($str)
	{
		$where	=	array(
			"email"			=>	$str
		);
		$cek	=	$this->crud_model->cek_data_where_array("users", $where);
		if ($cek) {
			$this->form_validation->set_message('email_check', 'Email Belum Terdaftar...');
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
