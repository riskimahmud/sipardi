<?php
class Promo extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    public function index()
    {
        $data['title']       =    "Promo";
        $data['data']        =    $this->crud_model->select_all_where_order('promo', 'kuota >', '0', 'promo_id', 'DESC');
        $data['page']        =    "promo/index";
        $this->load->view("frontend/main", $data);
    }
}
