<?php

defined('BASEPATH') or exit('No direct script access allowed');

$config = array(
	'produk'	=> array(
		array(
			'field' => 'nama_produk',
			'label' => 'Nama Produk',
			'rules' => 'trim|required|callback_check_nama'
		),
		array(
			'field' => 'kategori',
			'label' => 'Kategori',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'satuan',
			'label' => 'Satuan',
			'rules' => 'trim|required'
		),
		array(
			'field' => 'harga',
			'label' => 'Harga',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'diskon',
			'label' => 'Diskon',
			'rules' => 'trim|numeric'
		),
		array(
			'field' => 'berat',
			'label' => 'Berat',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'stok',
			'label' => 'Stok',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'gambar',
			'label' => 'Gambar',
			'rules' => 'trim|callback_check_file'
		),
	),
	'stok'	=> array(
		array(
			'field' => 'stok',
			'label' => 'Stok',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'keterangan',
			'label' => 'Keterangan',
			'rules' => 'trim'
		),
	),
	'cod'	=> array(
		array(
			'field' => 'harga',
			'label' => 'Harga',
			'rules' => 'trim|required|numeric'
		),
		array(
			'field' => 'kota_id',
			'label' => 'Kabupaten / Kota',
			'rules' => 'trim|callback_check_default|required'
		),
		array(
			'field' => 'kecamatan',
			'label' => 'Kecamatan',
			'rules' => 'trim|callback_check_default|required'
		),
		array(
			'field' => 'kelurahan',
			'label' => 'Kelurahan',
			'rules' => 'trim|callback_check_default|required'
		)
	),



);





/* End of file form_validation.php */

/* Location: ./application/config/form_validation.php */
