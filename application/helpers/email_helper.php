<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// fungsi untuk template email buat pesanan
function template_pesanan($data, $detail, $penerima)
{
	$pembeli = ambil_data_by_id_row("users", "id_konsumen", $data["id_pembeli"]);
	$penjual = ambil_data_by_id_row("penjual", "id_penjual", $data["id_penjual"]);
	$lokasi = ambil_data_by_id_row("rb_desa", "desa_id", $data["kelurahan_pembeli"]);
	if ($penerima == "pembeli") {
		$link = base_url("pembelian/detail/" . $data["kode_transaksi"]);
	} else {
		$link = base_url("penjualan/detail/" . $data["kode_transaksi"]);
	}
	$pesan = '<h5>Produk</h5>';
	$pesan .= '<table class="produk"><thead><tr><th>No.</th>';
	$pesan .= '<th>Nama Produk</th>';
	$pesan .= '<th>Qty</th>';
	$pesan .= '<th>Harga</th>';
	$pesan .= '<th>Jumlah</th>';
	$pesan .= '</tr></thead>';
	$pesan .= '<tbody>';
	$no = 1;
	foreach ($detail as $det) {
		$produk = ambil_data_by_id_row("produk", "id_produk", $det['id_produk']);
		$pesan .= '<td>' . $no++ . '</td>';
		$pesan .= '<td>' . $produk->nama_produk . '</td>';
		$pesan .= '<td>' . $det['qty'] . ' ' . $produk->satuan . '</td>';
		$pesan .= '<td>' . rupiah($det['harga']) . '</td>';
		$pesan .= '<td>' . rupiah($det['total']) . '</td>';
	}
	$pesan .= '</tbody></table>';

// 	$pesan .= '<table id="keterangan">';
// 	$pesan .= '<tr><td>Kode Transaksi</td><td class="value">' . $data["kode_transaksi"] . '</td><tr>';
// 	$pesan .= '<tr><td>Total Produk</td><td class="value">' . rupiah($data["total_produk"]) . '</td><tr>';
// 	$pesan .= '<tr><td>Ongkos Pengantaran</td><td class="value">' . rupiah($data["ongkir"]) . '</td><tr>';
// 	$pesan .= '<tr><td>Total Bayar</td><td class="value">' . rupiah($data["ongkir"] + $data["total_produk"]) . '</td><tr>';
// 	$pesan .= '<tr><td>Keterangan</td><td class="value">' . $data["keterangan_pembeli"] . '</td><tr>';
// 	$pesan .= '<tr><td>Alamat Pengantaran</td><td class="value">' . $data["alamat_pembeli"] . '. ';
// 	$pesan .= $lokasi->nama_desa . ', ' . get_kecamatan($lokasi->kecamatan_id) . ', ' . get_kota($lokasi->kota_id) . '</td></tr>';
// 	$pesan .= '</table>';

    $pesan .= '<table id="keterangan">';
	$pesan .= '<tr><td>Kode Transaksi</td><td class="value">' . $data["kode_transaksi"] . '</td><tr>';
	$pesan .= '<tr><td>Total Produk</td><td class="value">' . rupiah($data["total_produk"] - $data['potongan_barang']) . '</td><tr>';
	$pesan .= '<tr><td>Ongkos Pengantaran</td><td class="value">' . rupiah($data["ongkir"] - $data['potongan_ongkir']) . '</td><tr>';
	$pesan .= '<tr><td>Total Bayar</td><td class="value">' . rupiah($data["ongkir"] + $data["total_produk"] - ($data['potongan_ongkir'] + $data['potongan_barang'])) . '</td><tr>';
	$pesan .= '<tr><td>Keterangan</td><td class="value">' . $data["keterangan_pembeli"] . '</td><tr>';
	$pesan .= '<tr><td>Alamat Pengantaran</td><td class="value">' . $data["alamat_pembeli"] . '. ';
	$pesan .= $lokasi->nama_desa . ', ' . get_kecamatan($lokasi->kecamatan_id) . ', ' . get_kota($lokasi->kota_id) . '</td></tr>';
	$pesan .= '</table>';

	$pesan .= '<h5>Pembeli</h5><hr>';
	$pesan .= '<table id="keterangan">';
	$pesan .= '<tr><td>Nama</td><td class="value">' . $pembeli->nama_lengkap . '</td><tr>';
	$pesan .= '<tr><td>No. HP</td><td class="value">' . $pembeli->no_hp . '</td><tr>';
	$pesan .= '<tr><td>Alamat</td><td class="value">' . $pembeli->alamat_lengkap . '</td><tr>';
	$pesan .= '<tr><td>Kelurahan</td><td class="value">' . get_kelurahan($pembeli->kelurahan) . '</td><tr>';
	$pesan .= '<tr><td>Kecamatan</td><td class="value">' . get_kecamatan($pembeli->kecamatan) . '</td><tr>';
	$pesan .= '<tr><td>Kab / Kota</td><td class="value">' . get_kota($pembeli->kota_id) . '</td><tr>';
	$pesan .= '</table>';

	$pesan .= '<h5>Penjual</h5><hr>';
	$pesan .= '<table id="keterangan">';
	$pesan .= '<tr><td>Nama</td><td class="value">' . $penjual->nama_penjual . '</td><tr>';
	$pesan .= '<tr><td>No. HP</td><td class="value">' . $penjual->no_telpon . '</td><tr>';
	$pesan .= '<tr><td>Alamat</td><td class="value">' . $penjual->alamat_lengkap . '</td><tr>';
	$pesan .= '<tr><td>Kelurahan</td><td class="value">' . get_kelurahan($penjual->kelurahan) . '</td><tr>';
	$pesan .= '<tr><td>Kecamatan</td><td class="value">' . get_kecamatan($penjual->kecamatan) . '</td><tr>';
	$pesan .= '<tr><td>Kab / Kota</td><td class="value">' . get_kota($penjual->kota_id) . '</td><tr>';
	$pesan .= '</table>';

	$pesan .= '<a href="' . $link . '" class="button">Klik Disini Untuk Lihat Di Sipardi</a>';

	return $pesan;
}
