<?php
class Sms
{
    public function kirimSms($nomor, $pesan)
    {
        ob_start();
        // setting 
        $apikey      = '86dce9f2092d32bc49f93303f3e3e8a4'; // api key 
        $urlendpoint = 'http://sms114.xyz/sms/api_sms_otp_send_json.php'; // url endpoint api
        $callbackurl = ''; // url callback get status sms 

        // create header json  
        $senddata = array(
            'apikey' => $apikey,
            'callbackurl' => $callbackurl,
            'datapacket' => array()
        );

        // create detail data json 
        // data 1
        $number = '082347996363';
        $message = 'Message 1';
        array_push($senddata['datapacket'], array(
            'number' => trim($nomor),
            'message' => $pesan
        ));
        // sending  
        $data = json_encode($senddata);
        $curlHandle = curl_init($urlendpoint);
        curl_setopt($curlHandle, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $curlHandle,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, 30);
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 30);
        $respon = curl_exec($curlHandle);
        curl_close($curlHandle);
        // header('Content-Type: application/json');
        // echo $respon;
    }
}
